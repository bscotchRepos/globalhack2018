# Global Hack 7 (2018) - Butterscotch Shenanigans submission

This is a prototype made for Global Hack 7 (2018) by Butterscotch Shenanigans.

## How to run the prototype
+ Install [NodeJS version 8.x](https://nodejs.org/en/)
+ Make sure you have the latest stable version of npm (version 6.4.1): `npm install -g npm@latest`
+ Globally install nodemon: `npm install -g nodemon`
+ Install [MongoDB-CE version 3.6.x](https://www.mongodb.com/download-center?initial=true#previous)
+ The dependencies of the API and Site may need to be updated after you pull: close the software if it's running in a terminal, then run `./update-dependencies.sh`

If everything is installed and up to date you're ready to RUN. To do that you'll need three consoles open (one for the site, one for the API server, and one for mongo):

+ In one terminal
  + `cd site`
  + `npm run serve`
  + Then open up localhost:8080 your browser
+ In the other terminal
  + `cd api`
  + `npm start`
+ Run Mongo in the last terminal
  + `mongod`

Both the site and API have hot-reload, so any changes you make will cause an automatic restart.

## Contributing
This is a prototype project by Butterscotch Shenanigans for the Bscotch Global Hack team. The Bscotch team may contribute via pull requests. We are not looking for outside contributions.
