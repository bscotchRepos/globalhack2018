import {library} from '@fortawesome/fontawesome-svg-core';

import {
  faCog,
  faSignInAlt,
  faSignOutAlt,
  faBook,
  faChevronCircleRight,
  faQuestionCircle,
  faAtlas,
  faComments,
  faCaretUp,
  faCaretDown,
  faTimesCircle,
  faVolumeUp,
  faMicrophone,
  faClock,
  faBell,
  faLanguage,
  faSearch,
  faStopCircle,
  faCaretRight
} from '@fortawesome/free-solid-svg-icons';



library.add(
  faCog,
  faSignInAlt,
  faSignOutAlt,
  faBook,
  faChevronCircleRight,
  faQuestionCircle,
  faAtlas,
  faComments,
  faCaretUp,
  faCaretDown,
  faTimesCircle,
  faVolumeUp,
  faMicrophone,
  faClock,
  faBell,
  faLanguage,
  faSearch,
  faStopCircle,
  faCaretRight
);
