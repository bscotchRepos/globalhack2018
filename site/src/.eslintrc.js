module.exports = {
  root: true,
  env: {
    node: true,
    browser:true
  },
  'extends': [
    'plugin:vue/essential',
  ],
  rules: {
    'no-console': process.env.NODE_ENV === 'production' ? 'error' : 'off',
    'no-debugger': process.env.NODE_ENV === 'production' ? 'error' : 'off'
  },
  plugins:[
    'html','import','vue'
  ],
  parserOptions: {
    parser: 'babel-eslint'
  }
}