export default {
  methods:{
    flagUrl(country,size='550'){
      return `/img/flags/flags${size}/${country}.png`;
    }
  }
};
