export default {
  computed:{
    postType(){
      return this.$route.params.postType;
    },
    isQuestion(){
      return this.postType=='questions';
    },
    isGuide(){
      return this.postType=='guides';
    }
  }
}
