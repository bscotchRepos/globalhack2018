export default {
  data(){
    return {
      user: {},
      userInterval: null
    };
  },
  created(){
    this.loadUser();
    this.userInterval = setInterval(this.loadUser,200);
  },
  destroyed(){
    clearInterval(this.userInterval);
  },
  methods:{
    loadUser(){
      try{
        this.user = localStorage.getItem('user');
        if(this.user){
          this.user = JSON.parse(this.user);
        }
      }
      catch(err){
        this.logOut();
      }
    },
    saveUser(){
      localStorage.setItem('user',JSON.stringify(this.user));
    },
    logIn(username){
      this.user = this.user||{};
      this.user.username = username ;
      this.saveUser();
    },
    logOut(){
      this.user = '';
      localStorage.removeItem('user');
    },
    setTo(country){
      this.user = this.user || {} ;
      this.user.to = country || this.user.to;
      this.saveUser();
    },
    setFrom(country){
      this.user = this.user || {} ;
      this.user.from = country || this.user.from;
      this.saveUser();
    },
    setLanguage(lang){
      this.user = this.user || {} ;
      this.user.language = lang || this.user.language;
      this.saveUser();
    }
  }
}
