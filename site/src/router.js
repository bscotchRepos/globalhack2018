import Vue from 'vue'
import Router from 'vue-router'
import Home from './views/Home.vue'
import Login from './views/Login.vue'
import Logout from './views/Logout.vue'
import Where from './views/Where.vue'
import Language from './views/Language.vue'
import Posts from './views/Posts.vue'
import Post from './views/Post.vue'
import EditPost from './views/EditPost.vue'
import PostsDisplayMain from './views/PostsDisplayMain.vue'
Vue.use(Router)

export default new Router({
  mode: 'history',
  base: process.env.BASE_URL,
  routes: [
    {
      path: '/',
      name: 'home',
      component: Home
    },
    {
      path: '/from/:from/to/:to/lang/:language/posts',
      name: 'Posts',
      component: Posts,
      children:[
        {
          path:':postType/@edit/:postId?',
          name:'EditPost',
          component: EditPost
        },
        {
          path:':postType/:postId',
          name:'Post',
          component: Post
        },
        {
          path:':postType',
          name:'PostsDisplayMain',
          component: PostsDisplayMain
        }
      ]
    },
    {
      path: '/from/:from/to/:to/lang/:language?',
      name: 'Language',
      component: Language
    },
    {
      path: '/from/:from/to/:to?',
      name: 'WhereTo',
      component: Where
    },
    {
      path: '/from/:from?',
      name: 'WhereFrom',
      component: Where
    },
    {
      path: '/login',
      name: 'login',
      component: Login
    },
    {
      path: '/logout',
      name: 'logout',
      component: Logout
    },
    {
      path: '*',
      name: 'default',
      component: Home
    }
  ]
})
