export default {
    "ab": {
        "names": {
            "en": "Abkhazian",
            "ab": "аҧсуа бызшәа, аҧсшәа"
        }
    },
    "aa": {
        "names": {
            "en": "Afar",
            "aa": "Afaraf"
        }
    },
    "af": {
        "names": {
            "en": "Afrikaans",
            "af": "Afrikaans"
        }
    },
    "ak": {
        "names": {
            "en": "Akan",
            "ak": "Akan"
        }
    },
    "sq": {
        "names": {
            "en": "Albanian",
            "sq": "Shqip"
        }
    },
    "am": {
        "names": {
            "en": "Amharic",
            "am": "አማርኛ"
        }
    },
    "ar": {
        "names": {
            "en": "Arabic",
            "ar": "العربية"
        }
    },
    "an": {
        "names": {
            "en": "Aragonese",
            "an": "aragonés"
        }
    },
    "hy": {
        "names": {
            "en": "Armenian",
            "hy": "Հայերեն"
        }
    },
    "as": {
        "names": {
            "en": "Assamese",
            "as": "অসমীয়া"
        }
    },
    "av": {
        "names": {
            "en": "Avaric",
            "av": "авар мацӀ, магӀарул мацӀ"
        }
    },
    "ae": {
        "names": {
            "en": "Avestan",
            "ae": "avesta"
        }
    },
    "ay": {
        "names": {
            "en": "Aymara",
            "ay": "aymar aru"
        }
    },
    "az": {
        "names": {
            "en": "Azerbaijani",
            "az": "azərbaycan dili"
        }
    },
    "bm": {
        "names": {
            "en": "Bambara",
            "bm": "bamanankan"
        }
    },
    "ba": {
        "names": {
            "en": "Bashkir",
            "ba": "башҡорт теле"
        }
    },
    "eu": {
        "names": {
            "en": "Basque",
            "eu": "euskara, euskera"
        }
    },
    "be": {
        "names": {
            "en": "Belarusian",
            "be": "беларуская мова"
        }
    },
    "bn": {
        "names": {
            "en": "Bengali",
            "bn": "বাংলা"
        }
    },
    "bh": {
        "names": {
            "en": "Bihari languages",
            "bh": "भोजपुरी"
        }
    },
    "bi": {
        "names": {
            "en": "Bislama",
            "bi": "Bislama"
        }
    },
    "bs": {
        "names": {
            "en": "Bosnian",
            "bs": "bosanski jezik"
        }
    },
    "br": {
        "names": {
            "en": "Breton",
            "br": "brezhoneg"
        }
    },
    "bg": {
        "names": {
            "en": "Bulgarian",
            "bg": "български език"
        }
    },
    "my": {
        "names": {
            "en": "Burmese",
            "my": "ဗမာစာ"
        }
    },
    "ca": {
        "names": {
            "en": "Catalan, Valencian",
            "ca": "català, valencià"
        }
    },
    "ch": {
        "names": {
            "en": "Chamorro",
            "ch": "Chamoru"
        }
    },
    "ce": {
        "names": {
            "en": "Chechen",
            "ce": "нохчийн мотт"
        }
    },
    "ny": {
        "names": {
            "en": "Chichewa, Chewa, Nyanja",
            "ny": "chiCheŵa, chinyanja"
        }
    },
    "zh": {
        "names": {
            "en": "Chinese",
            "zh": "中文 (Zhōngwén), 汉语, 漢語"
        }
    },
    "cv": {
        "names": {
            "en": "Chuvash",
            "cv": "чӑваш чӗлхи"
        }
    },
    "kw": {
        "names": {
            "en": "Cornish",
            "kw": "Kernewek"
        }
    },
    "co": {
        "names": {
            "en": "Corsican",
            "co": "corsu, lingua corsa"
        }
    },
    "cr": {
        "names": {
            "en": "Cree",
            "cr": "ᓀᐦᐃᔭᐍᐏᐣ"
        }
    },
    "hr": {
        "names": {
            "en": "Croatian",
            "hr": "hrvatski jezik"
        }
    },
    "cs": {
        "names": {
            "en": "Czech",
            "cs": "čeština, český jazyk"
        }
    },
    "da": {
        "names": {
            "en": "Danish",
            "da": "dansk"
        }
    },
    "dv": {
        "names": {
            "en": "Divehi, Dhivehi, Maldivian",
            "dv": "ދިވެހި"
        }
    },
    "nl": {
        "names": {
            "en": "Dutch, Flemish",
            "nl": "Nederlands, Vlaams"
        }
    },
    "dz": {
        "names": {
            "en": "Dzongkha",
            "dz": "རྫོང་ཁ"
        }
    },
    "en": {
        "names": {
            "en": "English"
        }
    },
    "eo": {
        "names": {
            "en": "Esperanto",
            "eo": "Esperanto"
        }
    },
    "et": {
        "names": {
            "en": "Estonian",
            "et": "eesti, eesti keel"
        }
    },
    "ee": {
        "names": {
            "en": "Ewe",
            "ee": "Eʋegbe"
        }
    },
    "fo": {
        "names": {
            "en": "Faroese",
            "fo": "føroyskt"
        }
    },
    "fj": {
        "names": {
            "en": "Fijian",
            "fj": "vosa Vakaviti"
        }
    },
    "fi": {
        "names": {
            "en": "Finnish",
            "fi": "suomi, suomen kieli"
        }
    },
    "fr": {
        "names": {
            "en": "French",
            "fr": "français, langue française"
        }
    },
    "ff": {
        "names": {
            "en": "Fulah",
            "ff": "Fulfulde, Pulaar, Pular"
        }
    },
    "gl": {
        "names": {
            "en": "Galician",
            "gl": "Galego"
        }
    },
    "ka": {
        "names": {
            "en": "Georgian",
            "ka": "ქართული"
        }
    },
    "de": {
        "names": {
            "en": "German",
            "de": "Deutsch"
        }
    },
    "el": {
        "names": {
            "en": "Greek (modern)",
            "el": "ελληνικά"
        }
    },
    "gn": {
        "names": {
            "en": "Guaraní",
            "gn": "Avañe'ẽ"
        }
    },
    "gu": {
        "names": {
            "en": "Gujarati",
            "gu": "ગુજરાતી"
        }
    },
    "ht": {
        "names": {
            "en": "Haitian, Haitian Creole",
            "ht": "Kreyòl ayisyen"
        }
    },
    "ha": {
        "names": {
            "en": "Hausa",
            "ha": "(Hausa) هَوُسَ"
        }
    },
    "he": {
        "names": {
            "en": "Hebrew (modern)",
            "he": "עברית"
        }
    },
    "hz": {
        "names": {
            "en": "Herero",
            "hz": "Otjiherero"
        }
    },
    "hi": {
        "names": {
            "en": "Hindi",
            "hi": "हिन्दी, हिंदी"
        }
    },
    "ho": {
        "names": {
            "en": "Hiri Motu",
            "ho": "Hiri Motu"
        }
    },
    "hu": {
        "names": {
            "en": "Hungarian",
            "hu": "magyar"
        }
    },
    "ia": {
        "names": {
            "en": "Interlingua",
            "ia": "Interlingua"
        }
    },
    "id": {
        "names": {
            "en": "Indonesian",
            "id": "Bahasa Indonesia"
        }
    },
    "ie": {
        "names": {
            "en": "Interlingue",
            "ie": "Interlingue"
        }
    },
    "ga": {
        "names": {
            "en": "Irish",
            "ga": "Gaeilge"
        }
    },
    "ig": {
        "names": {
            "en": "Igbo",
            "ig": "Asụsụ Igbo"
        }
    },
    "ik": {
        "names": {
            "en": "Inupiaq",
            "ik": "Iñupiaq, Iñupiatun"
        }
    },
    "io": {
        "names": {
            "en": "Ido",
            "io": "Ido"
        }
    },
    "is": {
        "names": {
            "en": "Icelandic",
            "is": "Íslenska"
        }
    },
    "it": {
        "names": {
            "en": "Italian",
            "it": "Italiano"
        }
    },
    "iu": {
        "names": {
            "en": "Inuktitut",
            "iu": "ᐃᓄᒃᑎᑐᑦ"
        }
    },
    "ja": {
        "names": {
            "en": "Japanese",
            "ja": "日本語 (にほんご)"
        }
    },
    "jv": {
        "names": {
            "en": "Javanese",
            "jv": "ꦧꦱꦗꦮ, Basa Jawa"
        }
    },
    "kl": {
        "names": {
            "en": "Kalaallisut, Greenlandic",
            "kl": "kalaallisut, kalaallit oqaasii"
        }
    },
    "kn": {
        "names": {
            "en": "Kannada",
            "kn": "ಕನ್ನಡ"
        }
    },
    "kr": {
        "names": {
            "en": "Kanuri",
            "kr": "Kanuri"
        }
    },
    "ks": {
        "names": {
            "en": "Kashmiri",
            "ks": "कश्मीरी, كشميري‎"
        }
    },
    "kk": {
        "names": {
            "en": "Kazakh",
            "kk": "қазақ тілі"
        }
    },
    "km": {
        "names": {
            "en": "Central Khmer",
            "km": "ខ្មែរ, ខេមរភាសា, ភាសាខ្មែរ"
        }
    },
    "ki": {
        "names": {
            "en": "Kikuyu, Gikuyu",
            "ki": "Gĩkũyũ"
        }
    },
    "rw": {
        "names": {
            "en": "Kinyarwanda",
            "rw": "Ikinyarwanda"
        }
    },
    "ky": {
        "names": {
            "en": "Kirghiz, Kyrgyz",
            "ky": "Кыргызча, Кыргыз тили"
        }
    },
    "kv": {
        "names": {
            "en": "Komi",
            "kv": "коми кыв"
        }
    },
    "kg": {
        "names": {
            "en": "Kongo",
            "kg": "Kikongo"
        }
    },
    "ko": {
        "names": {
            "en": "Korean",
            "ko": "한국어"
        }
    },
    "ku": {
        "names": {
            "en": "Kurdish",
            "ku": "Kurdî, کوردی‎"
        }
    },
    "kj": {
        "names": {
            "en": "Kuanyama, Kwanyama",
            "kj": "Kuanyama"
        }
    },
    "la": {
        "names": {
            "en": "Latin",
            "la": "latine, lingua latina"
        }
    },
    "lb": {
        "names": {
            "en": "Luxembourgish, Letzeburgesch",
            "lb": "Lëtzebuergesch"
        }
    },
    "lg": {
        "names": {
            "en": "Ganda",
            "lg": "Luganda"
        }
    },
    "li": {
        "names": {
            "en": "Limburgan, Limburger, Limburgish",
            "li": "Limburgs"
        }
    },
    "ln": {
        "names": {
            "en": "Lingala",
            "ln": "Lingála"
        }
    },
    "lo": {
        "names": {
            "en": "Lao",
            "lo": "ພາສາລາວ"
        }
    },
    "lt": {
        "names": {
            "en": "Lithuanian",
            "lt": "lietuvių kalba"
        }
    },
    "lu": {
        "names": {
            "en": "Luba-Katanga",
            "lu": "Kiluba"
        }
    },
    "lv": {
        "names": {
            "en": "Latvian",
            "lv": "latviešu valoda"
        }
    },
    "gv": {
        "names": {
            "en": "Manx",
            "gv": "Gaelg, Gailck"
        }
    },
    "mk": {
        "names": {
            "en": "Macedonian",
            "mk": "македонски јазик"
        }
    },
    "mg": {
        "names": {
            "en": "Malagasy",
            "mg": "fiteny malagasy"
        }
    },
    "ms": {
        "names": {
            "en": "Malay",
            "ms": "Bahasa Melayu, بهاس ملايو‎"
        }
    },
    "ml": {
        "names": {
            "en": "Malayalam",
            "ml": "മലയാളം"
        }
    },
    "mt": {
        "names": {
            "en": "Maltese",
            "mt": "Malti"
        }
    },
    "mi": {
        "names": {
            "en": "Maori",
            "mi": "te reo Māori"
        }
    },
    "mr": {
        "names": {
            "en": "Marathi",
            "mr": "मराठी"
        }
    },
    "mh": {
        "names": {
            "en": "Marshallese",
            "mh": "Kajin M̧ajeļ"
        }
    },
    "mn": {
        "names": {
            "en": "Mongolian",
            "mn": "Монгол хэл"
        }
    },
    "na": {
        "names": {
            "en": "Nauru",
            "na": "Dorerin Naoero"
        }
    },
    "nv": {
        "names": {
            "en": "Navajo, Navaho",
            "nv": "Diné bizaad"
        }
    },
    "nd": {
        "names": {
            "en": "North Ndebele",
            "nd": "isiNdebele"
        }
    },
    "ne": {
        "names": {
            "en": "Nepali",
            "ne": "नेपाली"
        }
    },
    "ng": {
        "names": {
            "en": "Ndonga",
            "ng": "Owambo"
        }
    },
    "nb": {
        "names": {
            "en": "Norwegian Bokmål",
            "nb": "Norsk Bokmål"
        }
    },
    "nn": {
        "names": {
            "en": "Norwegian Nynorsk",
            "nn": "Norsk Nynorsk"
        }
    },
    "no": {
        "names": {
            "en": "Norwegian",
            "no": "Norsk"
        }
    },
    "ii": {
        "names": {
            "en": "Sichuan Yi, Nuosu",
            "ii": "ꆈꌠ꒿ Nuosuhxop"
        }
    },
    "nr": {
        "names": {
            "en": "South Ndebele",
            "nr": "isiNdebele"
        }
    },
    "oc": {
        "names": {
            "en": "Occitan",
            "oc": "occitan, lenga d'òc"
        }
    },
    "oj": {
        "names": {
            "en": "Ojibwa",
            "oj": "ᐊᓂᔑᓈᐯᒧᐎᓐ"
        }
    },
    "cu": {
        "names": {
            "en": "Church Slavic",
            "cu": "ѩзыкъ словѣньскъ"
        }
    },
    "om": {
        "names": {
            "en": "Oromo",
            "om": "Afaan Oromoo"
        }
    },
    "or": {
        "names": {
            "en": "Oriya",
            "or": "ଓଡ଼ିଆ"
        }
    },
    "os": {
        "names": {
            "en": "Ossetian, Ossetic",
            "os": "ирон æвзаг"
        }
    },
    "pa": {
        "names": {
            "en": "Panjabi, Punjabi",
            "pa": "ਪੰਜਾਬੀ"
        }
    },
    "pi": {
        "names": {
            "en": "Pali",
            "pi": "पाऴि"
        }
    },
    "fa": {
        "names": {
            "en": "Persian",
            "fa": "فارسی"
        }
    },
    "pl": {
        "names": {
            "en": "Polish",
            "pl": "język polski, polszczyzna"
        }
    },
    "ps": {
        "names": {
            "en": "Pashto, Pushto",
            "ps": "پښتو"
        }
    },
    "pt": {
        "names": {
            "en": "Portuguese",
            "pt": "Português"
        }
    },
    "qu": {
        "names": {
            "en": "Quechua",
            "qu": "Runa Simi, Kichwa"
        }
    },
    "rm": {
        "names": {
            "en": "Romansh",
            "rm": "Rumantsch Grischun"
        }
    },
    "rn": {
        "names": {
            "en": "Rundi",
            "rn": "Ikirundi"
        }
    },
    "ro": {
        "names": {
            "en": "Romanian, Moldavian, Moldovan",
            "ro": "Română"
        }
    },
    "ru": {
        "names": {
            "en": "Russian",
            "ru": "русский"
        }
    },
    "sa": {
        "names": {
            "en": "Sanskrit",
            "sa": "संस्कृतम्"
        }
    },
    "sc": {
        "names": {
            "en": "Sardinian",
            "sc": "sardu"
        }
    },
    "sd": {
        "names": {
            "en": "Sindhi",
            "sd": "सिन्धी, سنڌي، سندھی‎"
        }
    },
    "se": {
        "names": {
            "en": "Northern Sami",
            "se": "Davvisámegiella"
        }
    },
    "sm": {
        "names": {
            "en": "Samoan",
            "sm": "gagana fa'a Samoa"
        }
    },
    "sg": {
        "names": {
            "en": "Sango",
            "sg": "yângâ tî sängö"
        }
    },
    "sr": {
        "names": {
            "en": "Serbian",
            "sr": "српски језик"
        }
    },
    "gd": {
        "names": {
            "en": "Gaelic, Scottish Gaelic",
            "gd": "Gàidhlig"
        }
    },
    "sn": {
        "names": {
            "en": "Shona",
            "sn": "chiShona"
        }
    },
    "si": {
        "names": {
            "en": "Sinhala, Sinhalese",
            "si": "සිංහල"
        }
    },
    "sk": {
        "names": {
            "en": "Slovak",
            "sk": "Slovenčina, Slovenský Jazyk"
        }
    },
    "sl": {
        "names": {
            "en": "Slovene",
            "sl": "Slovenski Jezik, Slovenščina"
        }
    },
    "so": {
        "names": {
            "en": "Somali",
            "so": "Soomaaliga, af Soomaali"
        }
    },
    "st": {
        "names": {
            "en": "Southern Sotho",
            "st": "Sesotho"
        }
    },
    "es": {
        "names": {
            "en": "Spanish",
            "es": "Español"
        }
    },
    "su": {
        "names": {
            "en": "Sundanese",
            "su": "Basa Sunda"
        }
    },
    "sw": {
        "names": {
            "en": "Swahili",
            "sw": "Kiswahili"
        }
    },
    "ss": {
        "names": {
            "en": "Swati",
            "ss": "SiSwati"
        }
    },
    "sv": {
        "names": {
            "en": "Swedish",
            "sv": "Svenska"
        }
    },
    "ta": {
        "names": {
            "en": "Tamil",
            "ta": "தமிழ்"
        }
    },
    "te": {
        "names": {
            "en": "Telugu",
            "te": "తెలుగు"
        }
    },
    "tg": {
        "names": {
            "en": "Tajik",
            "tg": "тоҷикӣ, toçikī, تاجیکی‎"
        }
    },
    "th": {
        "names": {
            "en": "Thai",
            "th": "ไทย"
        }
    },
    "ti": {
        "names": {
            "en": "Tigrinya",
            "ti": "ትግርኛ"
        }
    },
    "bo": {
        "names": {
            "en": "Tibetan",
            "bo": "བོད་ཡིག"
        }
    },
    "tk": {
        "names": {
            "en": "Turkmen",
            "tk": "Türkmen, Түркмен"
        }
    },
    "tl": {
        "names": {
            "en": "Tagalog",
            "tl": "Wikang Tagalog"
        }
    },
    "tn": {
        "names": {
            "en": "Tswana",
            "tn": "Setswana"
        }
    },
    "to": {
        "names": {
            "en": "Tongan (Tonga Islands)",
            "to": "Faka Tonga"
        }
    },
    "tr": {
        "names": {
            "en": "Turkish",
            "tr": "Türkçe"
        }
    },
    "ts": {
        "names": {
            "en": "Tsonga",
            "ts": "Xitsonga"
        }
    },
    "tt": {
        "names": {
            "en": "Tatar",
            "tt": "татар теле, tatar tele"
        }
    },
    "tw": {
        "names": {
            "en": "Twi",
            "tw": "Twi"
        }
    },
    "ty": {
        "names": {
            "en": "Tahitian",
            "ty": "Reo Tahiti"
        }
    },
    "ug": {
        "names": {
            "en": "Uighur, Uyghur",
            "ug": "ئۇيغۇرچە‎, Uyghurche"
        }
    },
    "uk": {
        "names": {
            "en": "Ukrainian",
            "uk": "Українська"
        }
    },
    "ur": {
        "names": {
            "en": "Urdu",
            "ur": "اردو"
        }
    },
    "uz": {
        "names": {
            "en": "Uzbek",
            "uz": "Oʻzbek, Ўзбек, أۇزبېك‎"
        }
    },
    "ve": {
        "names": {
            "en": "Venda",
            "ve": "Tshivenḓa"
        }
    },
    "vi": {
        "names": {
            "en": "Vietnamese",
            "vi": "Tiếng Việt"
        }
    },
    "vo": {
        "names": {
            "en": "Volapük",
            "vo": "Volapük"
        }
    },
    "wa": {
        "names": {
            "en": "Walloon",
            "wa": "Walon"
        }
    },
    "cy": {
        "names": {
            "en": "Welsh",
            "cy": "Cymraeg"
        }
    },
    "wo": {
        "names": {
            "en": "Wolof",
            "wo": "Wollof"
        }
    },
    "fy": {
        "names": {
            "en": "Western Frisian",
            "fy": "Frysk"
        }
    },
    "xh": {
        "names": {
            "en": "Xhosa",
            "xh": "isiXhosa"
        }
    },
    "yi": {
        "names": {
            "en": "Yiddish",
            "yi": "ייִדיש"
        }
    },
    "yo": {
        "names": {
            "en": "Yoruba",
            "yo": "Yorùbá"
        }
    },
    "za": {
        "names": {
            "en": "Zhuang, Chuang",
            "za": "Saɯ cueŋƅ, Saw cuengh"
        }
    },
    "zu": {
        "names": {
            "en": "Zulu",
            "zu": "isiZulu"
        }
    }
}