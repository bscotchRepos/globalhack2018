const countries = require('./countries');
const codes = Object.keys(countries);
const flagRoot = `../../public/img/flags/flags550/`;
const fs = require('fs');
const language_name_codes = {}
const country_name_codes = {};
const country_info_output_file = 'country_info.json';



//fs.writeFileSync('countries-new.json',JSON.stringify(countries));

// #region Clean up raw language Data
const raw_language_file = require('./language_raw_data');
const languages = {};
const language_output_file_name = 'language_info.json';
console.log("Parsing languages from the raw language file.")

language_array = raw_language_file.split("\n")

for(const line in language_array) {
  const this_line = language_array[line];
  const line_components = this_line.split("\t");

  const english_name = line_components[0];
  const local_name = line_components[1];
  const language_code = line_components[2];
  //console.log("   " + language_code + "|" + english_name + "|" + local_name)
  
  language_name_codes[english_name] = language_code;
  languages[language_code] = {}
  languages[language_code]["names"] = {}
  languages[language_code]["names"]["en"] = english_name
  languages[language_code]["names"][language_code] = local_name
}

fs.writeFileSync(language_output_file_name,JSON.stringify(languages));
console.log("Wrote out to " + language_output_file_name);
// #endregion

const country_info = {}
for(const code of codes){
  //console.log(code + ":" + countries[code])
  english_name = countries[code];
  country_name_codes[english_name] = code;

  country_info[code] = {}
  country_info[code]["names"] = {};
  country_info[code]["names"]["en"] = english_name; 
  country_info[code]["languages"] = []
  // country_info[code]["languages"].push("en")
}

// #region Country Information
const raw_country_names = require('./country_names_raw.js');
const country_name_file_split = raw_country_names.split("%c");

const missing_countries = []
var found_countries = 0;

for(const c of country_name_file_split){
  //console.log("================================")

  // Country name is on the first line.
  const this_country_info = c.split("\n");
  const this_country_name = this_country_info[0];

  if (this_country_name != undefined) {
    //console.log(this_country_name);

    if (!country_name_codes[this_country_name]) {
        missing_countries.push(this_country_name);    
    }
    else {
      const this_country_code = country_name_codes[this_country_name];

      for (let info_line = 1; info_line < this_country_info.length; info_line++) {
        this_line_split = this_country_info[info_line].split(")");
        for(const this_name of this_line_split) {
          const language_break = this_name.split("(");
          //console.log("LANGUAGE BREAK!")
          //console.log(language_break);

          if (language_break.length > 1) {
            if (language_break[1] != undefined) {
              const these_languages = language_break[1].split(", ")
              let this_country_name = language_break[0];
              if (this_country_name.indexOf(", ") == 0) {
                this_country_name = this_country_name.slice(2,this_country_name.length)                
              }
              
              // Check whether the country name is presented in two languages.
              if (this_country_name.indexOf(" - ") != -1) {
                  this_country_name = this_country_name.split(" - ")[1]
              }

              if (this_country_name.indexOf(" or ") != -1) {
                this_country_name = this_country_name.split(" or ")[0]
              }
                
              if (this_country_name.indexOf(" / ") != -1) {
                this_country_name = this_country_name.split(" or ")[0]
              }

              if (this_country_name[this_country_name.length-1] == " ") {
                this_country_name = this_country_name.slice(0,this_country_name.length-2);
              }
              //console.log(this_country_name)
              //console.log(these_languages)
              
              if (this_country_name != "") {
                for (const lang of these_languages) {
                  if (language_name_codes[lang]) {
                      const this_language_code = language_name_codes[lang];
                      country_info[this_country_code]["names"][this_language_code] = this_country_name;
                      //console.log("   " + language_name_codes[lang] + " | " + this_country_name)
                  }
                }
              }
            }
          }
        }
      }
    }
  }
}

//console.log("Missing countries:")
for (const c in missing_countries) {
  //console.log("    " + missing_countries[c]);
}

/*
{ country_code :
    { 
        "names" : { language : name },
        "languages": [ language0, language1 ]
    },
}
*/


// #endregion

// #region Country Languages
const country_languages_raw_filename = './country_languages_raw.js';
const country_languages_file = require(country_languages_raw_filename);
const country_languages = {};

console.log("Parsing country languages from " + country_languages_raw_filename)

const country_languages_file_split = country_languages_file.split("\n");

for (let i = 0; i < country_languages_file_split.length; i += 2) {
  let this_country_name = country_languages_file_split[i];
  console.log("=====" + this_country_name + "=====")
  if (country_name_codes[this_country_name]) {
    let this_country_code = country_name_codes[this_country_name];
    //console.log(country_languages_file_split[i] + " : " + country_languages_file_split[i+1]);

    let this_country_languages = country_languages_file_split[i+1].split(", ")
    //console.log(this_country_languages);
    for (const lang of this_country_languages) {
        if (language_name_codes[lang]) {
          country_info[this_country_code]["languages"].push(language_name_codes[lang])
        }
    }
  }
  else {
    // country_info[this_country_code]["languages"].push('en');
    console.log("Country not found:" + this_country_name)
  }
}

const countryCodes = Object.keys(country_info);
for( const code of countryCodes){
  const imgPath = `${flagRoot}${code}.png`;
  if(!fs.existsSync(imgPath)){
    Reflect.deleteProperty(countries,code);
  }
  else if( ! country_info[code]["languages"].length){
    console.log("Added English");
    country_info[code]["languages"].push('en');
  }
}

// #endregion

fs.writeFileSync(country_info_output_file,JSON.stringify(country_info));
console.log("Wrote out to " + country_info_output_file);