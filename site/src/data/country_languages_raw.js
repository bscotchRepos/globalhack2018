module.exports = `Afghanistan
Pashto, Dari, Uzbek, Turkmen, Balochi, Pashayi, Nuristani
Albania
Albanian, Italian, Greek, English
Algeria
Modern Standard Arabic, Tamazight, French
American Samoa
Samoan, English
Andorra
Catalan, Spanish, French, Portuguese
Angola
Portuguese, French, English
Anguilla
English
Antigua and Barbuda
English
Argentina
Spanish, English, Portuguese, Italian, French, German
Armenia
Armenian, English, Kurdish, Assyrian, Greek, Russian
Aruba
Dutch, English, Spanish, French, Portuguese
Australia
English
Austria
German, Austro-Bavarian, Alemannic, English, French, Italian
Azerbaijan
Azerbaijani, English, Russian
Bahamas
English
Bahrain
Arabic, Persian, Udru
Bangladesh
Bengali, English, Arabic
Barbados
English
Belarus
Belarusian, Russian, Polish, Ukrainian, Lithuanian
Belgium
Dutch, French, German
Belize
English, Spanish
Benin
French, Baatonum
Bermuda
English
Bhutan
Dzongkha, Nepali
Bolivia
Spanish
Bosnia and Herzegovina
Bosnian, Croatian, Serbian
Botswana
English, Setswana
Brazil
Portuguese
British Virgin Islands
English
Brunei
Malay, English
Bulgaria
Bulgarian, Turkish, Roma
Burkina Faso
French
Burundi
Kirundi, English, French
Cambodia
Khmer
Cameroon
French, English
Canada
English, French
Cabo Verde
Portuguese
Cayman Islands
English
Central African Republic
French, Sangho
Chad
French, Arabic
Chile
Spanish
China
Chinese
Christmas Island
English
Cocos (Keeling) Islands
Malay, English
Colombia
Spanish
Comoros
French, Comorian, Arabic
Cook Islands
English, Rarotongan
Costa Rica
Spanish
Croatia
Croatian
Cuba
Spanish
Cyprus
Turkish, Greek
Czech Republic
Czech
Democratic Republic of the Congo
French, Kituba, Lingala
Denmark
Danish, Faroese, Greenlandic, German
Djibouti
Arabic, French, Somali, Anfar
Dominica
English
Dominican Republic
Spanish
Ecuador
Spanish, Kichwa, Shuar
Egypt
Modern Standard Arabic, Arabic
El Salvador
Spanish
Equatorial Guinea
French, Spanish, Portuguese
Eritrea
Tigrinya, Arabic, English
Estonia
Estonian, Russian
Ethiopia
Amharic, Omoro
Falkland Islands
English
Faroe Islands
Faroese, Danish
Fiji
English, Fijan, Hindi
Finland
Finnish, Swedish
France
French
French Guiana
French, English
French Polynesia
French, Tahitian, English
Gabon
French
Gambia
English, Mandinka
Georgia
Georgian, Russian
Germany
German
Ghana
English
Gibraltar
English
Greece
Greek
Greenland
Greenlandic, Danish, English
Grenada
English, French
Guadeloupe
French
Guam
Chamorro, English
Guatemala
Spanish
Guinea
French, Fula, Malinké, Susu, Kissi, Kpelle, Toma
Guinea-Bissau
Portuguese, French
Guyana
English
Haiti
Hatian Creole, French
Honduras
Spanish
Hong Kong
Cantonese
Hungary
Hungarian
Iceland
Icelandic
India
Hindi, English, Tamil
Indonesia
Indonesian
Iran
Persian
Iraq
Arabic
Ireland
English, Irish
Isle of Man
English, Manx English
Israel
Hebrew, Arabic
Italy
Italian
Jamaica
English, Jamaican Patois
Japan
Japanese
Jersey
French, English
Jordan
Arabic
Kazakhstan
Kazakh, Russian
Kenya
Swahili, English
Kiribati
English, Kiribati
Kosovo
Albanian, Serbian
Kuwait
Arabic
Kyrgyzstan
Kyrgyz, Russian
Laos
Lao
Latvia
Latvian
Lebanon
Arabic, French
Lesotho
English, Southern Sotho
Liberia
English
Libya
Arabic
Liechtenstein
German
Lithuania
Lithuanian
Luxembourg
French, Luxembourgish, German
Madagascar
Malagasy, French
Malawi
Chewa, English
Malaysia
Malay
Maldives
Divehi
Mali
French
Malta
English, Maltese
Marshall Islands
English, Marshallese
Mauritania
Arabic
Mauritius
Mauritian Creole, English, French
Mayotte
French
Mexico
Spanish
Moldova
Romanian
Monaco
French
Mongolia
Mongolian
Montenegro
Montenegrin
Montserrat
English
Morocco
Arabic, Berber
Mozambique
Portuguese
Namibia
English
Nauru
English, Nauruan
Nepal
Nepali
Netherlands
Dutch, Frisian, English, Papiamento
New Caledonia
French
New Zealand
Māori, English
Nicaragua
Spanish, English
Niger
French
Nigeria
English, Hausa, Yoruba, Igbo, Ijaw, Tiv
Niue
English, Niue
Norfolk Island
English, Norfuk
North Korea
Korean
Northern Mariana Islands
English, Chamorro, Carolinian
Norway
Norwegian
Oman
Arabic
Pakistan
Urdu, English
Palau
English, Palauan
Panama
Spanish
Papua New Guinea
Tok Pisin, English, Hiri Motu
Paraguay
Paraguayan Guaraní, Spanish
Peru
Spanish, Aymara, Quechua
Philippines
English, Filipino
Pitcairn Islands
English, Pitkern
Poland
Polish
Portugal
Portuguese
Puerto Rico
Spanish, English
Qatar
Arabic
Romania
Romanian
Russia
Russian
Rwanda
Kinyarwanda, English, French
American Samoa
English, Samoan
San Marino
Italian
Saudi Arabia
Arabic
Senegal
French, Wolof
Serbia
Serbian
Seychelles
French, Seselwa, English
Sierra Leone
English, Bengali
Singapore
English, Malay, Tamil, Mandarin
Slovakia
Slovak
Slovenia
Slovene
Solomon Islands
English, Solomans Pijin
Somalia
Somali, Arabic
South Africa
Afrikaans, English, Ndebele, Northern Sotho, Sotho, Swazi, Tsonga, Tswana, Venda, Xhosa, Zula
South Korea
Korean
Spain
Spanish
Sri Lanka
Sinhala, Tamil, English
Sudan
Arabic, English
Suriname
Dutch
Swaziland
English, Swazi
Sweden
Swedish
Switzerland
German, French, Italian, Romansh
Syria
Arabic
Taiwan
Mandarin
Tajikistan
Tajiki, Russian, Persian
Tanzania
Swahili, English
Thailand
Thai
Togo
French
Tonga
English, Tongan
Trinidad and Tobago
English
Tunisia
Arabic
Turkey
Turkish
Turkmenistan
Turkmen, Russian
Turks and Caicos Islands
English
Tuvalu
English, Tuvaluan
Uganda
English, Swahili
Ukraine
Ukrainian
United Arab Emirates
Arabic
United Kingdom
English
United States of America
English, Spanish, Chinese, Hindi
Uruguay
Spanish
US Virgin Islands
English
Uzbekistan
Uzbek
Vanuatu
French, Bislama, English
Venezuela
Spanish
Vietnam
Vietnamese
Wallis and Futuna
French
Yemen
Arabic
Zambia
English
Zimbabwe
English, Ndebele, Shona`