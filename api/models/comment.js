const mongoose = require('mongoose');

var schema = new mongoose.Schema({
  author: {
    type: String,
    maxlength: 64,
    minlength: 1,
    required: true,
    index: true
  },
  postId:{
    type: mongoose.Schema.Types.ObjectId,
    required: true,
    index:true
  },
  date: {
    type: Date,
    default: Date.now,
    required:true
  },
  body:{
    type: String,
    maxlength: 20000,
  },
  best:Boolean,
  upvotes:{
    type: Number,
    min:0
  },
  downvotes:{
    type: Number,
    min:0
  },
});
module.exports = mongoose.model('comment', schema );
