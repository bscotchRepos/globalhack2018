const mongoose = require('mongoose');

var schema = new mongoose.Schema({
  author: {
    type: String,
    maxlength: 64,
    minlength: 1,
    required: true,
    index: true
  },
  lang:{
    type: String,
    required: true
  },
  type:{
    type: String,
    enum:['questions','guides'],
    index: true
  },
  date: {
    type: Date,
    default: Date.now,
    required:true
  },
  from:{
    type: String,
    maxlength: 2,
    minlength: 2,
    required: true,
    index: true
  },
  to:{
    type: String,
    maxlength: 2,
    minlength: 2,
    required: true,
    index: true
  },
  title:{
    type: String,
    maxlength: 140,
    minlength: 5,
    required: true,
    index: 'text'
  },
  body:{
    type: String,
    maxlength: 20000,
    minlength: 12,
    required: true
  },
  upvotes:{
    type: Number,
    min:0
  },
  downvotes:{
    type: Number,
    min:0
  },
  commentCount:{
    type: Number,
    min:0
  },
  hasAudio: Boolean
});
module.exports = mongoose.model('post', schema );
