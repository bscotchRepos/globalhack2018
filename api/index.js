const express = require('express');
const bodyParser = require('body-parser');
const app = express();
const port = 8081;
const posts = require('./posts');
const comments = require('./comments');

const mongoose = require('mongoose');
mongoose.connect('mongodb://localhost:27017/2flags');

require('./dummy_data/load')();

// Authentication
// (Just a stand-in!)
app.use((req,res,next)=>{
  res.setHeader('Allow','GET,HEAD,POST,PATCH,PUT,DELETE');
  res.setHeader('Access-Control-Allow-Origin','*');
  res.setHeader('Access-Control-Allow-Headers','*');
  req.user = req.headers.username;
  return next();
});

app.options('*',(req,res)=>{
  return res.sendStatus(200);
});

const requireLoggedIn = (req,res,next)=>{
  if(!req.user){ return res.sendStatus(403);}
  return next();
}

// Posts
app.get('/api/posts', posts.getPosts);
app.post('/api/posts',
  requireLoggedIn,
  bodyParser.json(),
  posts.createPost
);
app.get("/api/posts/:postId",posts.getPost);
app.patch("/api/posts/:postId",
  requireLoggedIn,
  bodyParser.json(),
  posts.updatePost
);

// Comments
app.get('/api/posts/:postId/comments',
  comments.getComments);
app.post('/api/posts/:postId/comments',
  requireLoggedIn,
  bodyParser.json(),
  comments.createComment
);
app.get("/api/posts/:postId/comments/:commentId",
  comments.getComment
);
app.patch("/api/posts/:postId/comments/:commentId",
  requireLoggedIn,
  bodyParser.json(),
  comments.updateComment
);


app.listen(port, () => console.log(`Listening on port ${port}!`));
