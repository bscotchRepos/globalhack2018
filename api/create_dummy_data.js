
const mongoose = require("mongoose")
const raw_posts = require('./dummy_data/dummy_posts_raw.js');
const raw_guides = require('./dummy_data/dummy_guides_raw.js');

const split_posts = raw_posts.split("\n");
const split_guides = raw_guides.split("\n");
const fs = require('fs');

const compiled_dummy_posts = [];
const compiled_dummy_comments = [];

let getRandomName = function() {
    names = [
        "Bob",
        "Susan",
        "Charles",
        "Cassandra",
        "Alfie",
        "Vanessa",
        "Raul",
        "Chad",
        "Dennis",
        "Lakshmi",
        "Juan",
        "Sara",
        "Nicholas",
        "Sven",
        "Anuja",
        "Rupert",
        "Tanya",
        "Jolyn",
        "Deb",
        "Jere",
        "Jayne",
        "Williams",
        "Ilse",
        "Gennie",
        "Aida",
        "Veda",
        "Rosalee",
        "Leopoldo",
        "Max",
        "Neil",
        "Nina",
        "Suzette",
        "Charlyn",
        "Ina",
        "Takisha",
        "Cecila",
        "Tomi",
        "Alva",
        "Russel",
        "Kiera",
        "Regan",
        "Sharika",
        "Earlean",
        "Carmelo",
        "Marty",
        "Oneida",
        "Jewel",
        "Alissa",
        "Evelynn",
        "Mckinley",
        "Lashaunda",
        "Blanch",
        "Orville",
        "Rogelio",
        "Leena",
        "Ariel",
        "Carylon",
        "Darleen",
        "Katherine",
        "Dena",
        "Dannie",
        "Amado",
        "Herb",
        "Winford",
        "Sharleen"
    ]

    return names[Math.floor(Math.random()*(names.length-1))];
}

let getRandomComment = function() {
   var random_comments = [
        "This is amazing!",
        "This is a life saver.",
        "This guide is thorough and well-researched. Great work!",
        "I wish I had know this when I moved here!",
        "YES. This is great information!",
        "Agreed with all of this.",
        "This guide is very well put together.",
        "I'm sending this to my cousin. He's been asking me about this!",
        "This is insightful!",
        "I've been living here for 10 years and I didn't know some of this!",
        "This is going to be really useful for people. Great job!",
        "If I had this guide when I moved here, I would have landed on my feet!",
        "This is going to help a lot of people.",
        "I like your angle on this. I hadn't thought about it that way!",
        "This is some great advice.",
        "YES!",
        "Nice! Well done!",
        "This is really well written!",
        "I can't believe I didn't know this!",
        "I'm glad this information is getting out there.",
        "I hadn't considered making a guide for this, but now that I see it, I wish I had!"
    ]
    return random_comments[Math.floor(Math.random()*(random_comments.length-1))];
}

var current_post_id = mongoose.Types.ObjectId();
var most_recent_post = 0;

for (let post of split_posts) {
    let post_display = post;
    if (post_display.indexOf('\t') >= 0) {
        post_display = post_display.replace("\t", "");
        //console.log("Comment: " + post_display)
        // Create a comment.
        var this_comment = {}

        this_comment["author"] = getRandomName();
        this_comment["postId"] = current_post_id;
        this_comment["body"] = post_display;
        this_comment["lang"] = "en";
        this_comment["upvotes"] = Math.floor(Math.random()*1000)+200;
        this_comment["downvotes"] = Math.floor(Math.random()*200);
        most_recent_post["commentCount"] += 1;
        compiled_dummy_comments.push(this_comment);
    }
    else {
        current_post_id = mongoose.Types.ObjectId();    
        //console.log("Post: " + post_display);
        // Create a comment.
        var this_post = {};

        this_post["author"] = getRandomName();
        this_post["_id"] = current_post_id;
        this_post["body"] = post_display;
        this_post["lang"] = "en";
        this_post["upvotes"] = Math.floor(Math.random()*1000)+200;
        this_post["downvotes"] = Math.floor(Math.random()*200);
        this_post["commentCount"] = 0;
        this_post["type"] = "questions";
        this_post["title"] = post_display;
        this_post["from"] = "gb";
        this_post["to"] = "us";
        most_recent_post = this_post;
        compiled_dummy_posts.push(this_post)
    }
}

for (var i = 0; i < split_guides.length; i += 2) {
  let post_title = split_guides[i];
  let post_body = split_guides[i+1].replace("\t", "");

  current_post_id = mongoose.Types.ObjectId();    
  var this_post = {};

  this_post["author"] = getRandomName();
  this_post["_id"] = current_post_id;
  this_post["body"] = post_body;
  this_post["lang"] = "en";
  this_post["upvotes"] = Math.floor(Math.random()*1000)+200;
  this_post["downvotes"] = Math.floor(Math.random()*200);
  this_post["commentCount"] = 0;
  this_post["type"] = "guides";
  this_post["title"] = post_title;
  this_post["from"] = "gb";
  this_post["to"] = "us";
  most_recent_post = this_post;
  compiled_dummy_posts.push(this_post)

  // Create a comment.
  var num_random_comments = Math.floor(Math.random()*5)+1;

  for (var c = 0; c < num_random_comments; c++) {
    var this_comment = {}

    this_comment["author"] = getRandomName();
    this_comment["postId"] = current_post_id;
    this_comment["body"] = getRandomComment();
    this_comment["lang"] = "en";
    this_comment["upvotes"] = Math.floor(Math.random()*1000)+200;
    this_comment["downvotes"] = Math.floor(Math.random()*200);
    most_recent_post["commentCount"] += 1;
    compiled_dummy_comments.push(this_comment);
  }
}

fs.writeFileSync('./dummy_data/dummy_posts.js', JSON.stringify(compiled_dummy_posts));
fs.writeFileSync('./dummy_data/dummy_comments.js', JSON.stringify(compiled_dummy_comments));