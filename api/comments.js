const Comment = require('./models/comment');

const getComments = async (req, res) => {
  try{
    res.status(200).send({comments: await Comment.find({postId:req.params.postId})});
  }
  catch(err){
    console.log(err);
    return res.sendStatus(500);
  }
};

const createComment = async (req, res) => {
  try{
    req.body.postId = req.params.postId ;
    await (new Comment(req.body)).save();
    return res.sendStatus(204);
  }
  catch(err){
    console.log(err);
    return res.sendStatus(500);
  }
};

const getComment = async (req,res)=>{
  try{
    const comment = await Comment.findOne({_id:req.params.commentId,postId:req.params.postId});
    if(!comment){ return res.status(404); }
    res.status(200).send(comment);
  }
  catch(err){
    console.log(err);
    return res.sendStatus(500);
  }
};

const updateComment = async (req,res)=>{
  // If ! is the author, allow an upvote
  // If is the author, allow marking as BEST
  try{
    if(req.body.upvote){
      // Then must not be the comment author
      const comment = await Comment.findOne({_id:req.params.commentId,postId:req.params.postId});
      if(!comment){ return res.status(404); }
      if(comment.author != req.user){
        await comment.update({$inc:{upvotes:1}});
        return res.sendStatus(200);
      }
      return res.sendStatus(409);
    }
    else if(req.body.best){
      // Then must be the original post author
      const post = await Post.findOne({_id:req.params.postId});
      if(!post){ return res.status(404); }
      if(post.author == req.user){
        // Unset any other bests, then set this one
        await Comment.update({postId:req.params.postId,best:true},{$unset:{best:true}});
        await Comment.update({_id:req.params.commentId},{best:true});
        return res.sendStatus(200);
      }
    }
    else{
      return res.sendStatus(400);
    }
  }
  catch(err){
    console.log(err);
    return res.sendStatus(500);
  }
};

module.exports = {
  getComments,
  createComment,
  getComment,
  updateComment
};
