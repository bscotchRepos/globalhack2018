const Post = require('./models/post');

const getPosts = async (req, res) => {
  try{
    const search = {} ;
    ['from','to','type'].forEach(field=>{
      if(req.query[field]){
        search[field] = req.query[field];
      }
    });
    if(req.query.search){
      search.$text = {
        $search: req.query.search
      };
    }
    let sort={upvotes:-1};
    if(req.query.sort=='new'){
      sort = {createdAt:-1};
    }
    res.status(200).send({posts: await Post.find(search,'-body').sort(sort)});
  }
  catch(err){
    console.log(err);
    return res.sendStatus(500);
  }
};

const createPost = async (req, res) => {
  try{
    const newPost = new Post(req.body);
    await newPost.save();
    return res.send({postId:newPost._id});
  }
  catch(err){
    console.log(err);
    return res.sendStatus(500);
  }
};

const getPost = async (req,res)=>{
  try{
    const post = await Post.findOne({_id:req.params.postId});
    if(!post){ return res.status(404); }
    res.status(200).send(post);
  }
  catch(err){
    console.log(err);
    return res.sendStatus(500);
  }
};

const updatePost = async (req,res)=>{
  // If ! is the author, allow an upvote
  try{
    const post = await Post.findOne({_id:req.params.postId});
    if(!post){ return res.status(404); }
    if(post.author != req.user){
      await post.update({$inc:{upvotes:1}});
      return res.sendStatus(200);
    }
    else{
      return res.sendStatus(409);
    }
  }
  catch(err){
    console.log(err);
    return res.sendStatus(500);
  }
};

module.exports = {
  getPosts,
  createPost,
  getPost,
  updatePost
};
