const Post = require('../models/post');
const Comment = require('../models/comment');
const posts = require('./dummy_posts_translated');
const comments = require('./dummy_comments_translated');
const mongoose = require('mongoose');

const load = async ()=>{
  await Comment.remove({});
  for(const comment of comments){
    const newComment = new Comment(comment);
    await newComment.save();
  }
  await Post.remove({});
  for(const post of posts){
    const newPost = new Post(post);
    await newPost.save();
  }
};

module.exports = load ;
