module.exports = [
    {
        "author": "Ariel",
        "postId": "5bc2a2acf3fa222500895f62",
        "body": "You can go to any bank in your area and talk to a banker there. I'd recommend using Google Maps to find the nearest bank to you. If you see a bank called a \"Community Credit Union\", that means it's a smaller, local bank, while banks that aren't called \"Credit Union\" or similar will probably be branches of larger, national banks. Whichever bank you choose probably doesn't matter that much. You'll need a small amount of money to open an account. Once you have the account, you will be able to order checks directly from the bank, get a credit card, and set up other accounts as well. All you need to do is ask the banker for any of these services, and they will get you set up.",
        "lang": "en",
        "upvotes": 254,
        "downvotes": 2
    },
    {
        "author": "Vanessa",
        "postId": "5bc2a2acf3fa222500895f63",
        "body": "You will probably need a bank account. Employers will usually pay by checks, which you need to bring to a bank to deposit, or they will pay by \"direct deposit,\" which means they will electronically transfer money into your bank account.",
        "lang": "en",
        "upvotes": 950,
        "downvotes": 17
    },
    {
        "author": "Russel",
        "postId": "5bc2a2acf3fa222500895f64",
        "body": "Go to USCIS.gov. That's the web site for United States Citizenship and Immigration Services, and they should have all the info you need.",
        "lang": "en",
        "upvotes": 882,
        "downvotes": 51
    },
    {
        "author": "Tanya",
        "postId": "5bc2a2acf3fa222500895f65",
        "body": "Find a CPA in your area who specializes in personal taxes. CPA stands for Certified Public Accountant. Generally, you will pay a CPA something like a couple hundred dollars to go through your finances and make sure you have reported everything properly.",
        "lang": "en",
        "upvotes": 1001,
        "downvotes": 15
    },
    {
        "author": "Alfie",
        "postId": "5bc2a2acf3fa222500895f65",
        "body": "Taxes are due April 15, although you can file for an extension. Everything is way easier if you get a good accountant, though, so I'd recommend starting there!",
        "lang": "en",
        "upvotes": 241,
        "downvotes": 154
    },
    {
        "author": "Winford",
        "postId": "5bc2a2acf3fa222500895f65",
        "body": "There are lots of different kinds of taxes, but many of them happen automatically. For example, you will pay sales tax on lots of transactions, like when you buy items at stores. That tax happens automatically, and you don't have to do anything about it. The big one is the Federal Income Tax, which you'll want to get a CPA to help you with.",
        "lang": "en",
        "upvotes": 1143,
        "downvotes": 66
    },
    {
        "author": "Suzette",
        "postId": "5bc2a2acf3fa222500895f66",
        "body": "The first place to look is an online site like ziprecruiter.com or indeed.com. Lots of companies post a huge range of jobs there. When you apply, you will need to create a resume and a cover letter. The resume is a document that is a concise detailing of who you are, what your skills are, and your personal history. The cover letter is one-page letter that introduces you to the company and explains why you think they should hire you.",
        "lang": "en",
        "upvotes": 811,
        "downvotes": 96
    },
    {
        "author": "Susan",
        "postId": "5bc2a2acf3fa222500895f66",
        "body": "Make sure your visa status is in order and that you are legally eligible to work in the country. Some employers worry about that, so if you can anticipate that and be very up-front about it right at the start, you'll have an advantage.",
        "lang": "en",
        "upvotes": 757,
        "downvotes": 170
    },
    {
        "author": "Cassandra",
        "postId": "5bc2a2acf3fa222500895f67",
        "body": "Sometimes you will rent an apartment that will have a washer and dryer in the building, or in the apartment complex. In those cases, you'll usually need to bring some quarters to feed into the machine to wash each load. If you don't have access to that, you can bring your clothes to a laundromat, which is a shop that has a whole bunch of washers and dryers init. You'll need to bring quarters there too, probably.",
        "lang": "en",
        "upvotes": 781,
        "downvotes": 146
    },
    {
        "author": "Raul",
        "postId": "5bc2a2acf3fa222500895f67",
        "body": "You're going to need a lot of quarters. Ideally, ask for rolls of quarters when you go to a bank. If you can't make it to a bank, you can sometimes go to the customer service desk at a grocery store, and they'll get you the quarters -- but not always.",
        "lang": "en",
        "upvotes": 542,
        "downvotes": 73
    },
    {
        "author": "Rupert",
        "postId": "5bc2a2acf3fa222500895f68",
        "body": "If you don't have a car, you'll want to use public transportation. The best way to figure that out is to Google the website for the Department of Transportation for that state or city, and they should have a bunch of information about it. Google Maps now also has public transportation options, which can help you figure out which bus stops you need to go to, or which train stations.",
        "lang": "en",
        "upvotes": 806,
        "downvotes": 191
    },
    {
        "author": "Vanessa",
        "postId": "5bc2a2acf3fa222500895f69",
        "body": "Most small towns don't have much for public transportation, so you'll need to either get a car, make friends who have cars, carpool to work, or try to find a place close enough to your work that you can walk or bike.",
        "lang": "en",
        "upvotes": 1043,
        "downvotes": 70
    },
    {
        "author": "Takisha",
        "postId": "5bc2a2acf3fa222500895f6a",
        "body": "I can't believe I didn't know this!",
        "lang": "en",
        "upvotes": 202,
        "downvotes": 154
    },
    {
        "author": "Carmelo",
        "postId": "5bc2a2acf3fa222500895f6a",
        "body": "Nice! Well done!",
        "lang": "en",
        "upvotes": 350,
        "downvotes": 143
    },
    {
        "author": "Alfie",
        "postId": "5bc2a2acf3fa222500895f6a",
        "body": "YES!",
        "lang": "en",
        "upvotes": 376,
        "downvotes": 134
    },
    {
        "author": "Alissa",
        "postId": "5bc2a2acf3fa222500895f6a",
        "body": "I can't believe I didn't know this!",
        "lang": "en",
        "upvotes": 687,
        "downvotes": 139
    },
    {
        "author": "Nina",
        "postId": "5bc2a2acf3fa222500895f6a",
        "body": "This guide is very well put together.",
        "lang": "en",
        "upvotes": 405,
        "downvotes": 154
    },
    {
        "author": "Nicholas",
        "postId": "5bc2a2acf3fa222500895f6b",
        "body": "YES!",
        "lang": "en",
        "upvotes": 876,
        "downvotes": 86
    },
    {
        "author": "Blanch",
        "postId": "5bc2a2acf3fa222500895f6b",
        "body": "This is really well written!",
        "lang": "en",
        "upvotes": 609,
        "downvotes": 25
    },
    {
        "author": "Jere",
        "postId": "5bc2a2acf3fa222500895f6b",
        "body": "I'm glad this information is getting out there.",
        "lang": "en",
        "upvotes": 1187,
        "downvotes": 175
    },
    {
        "author": "Amado",
        "postId": "5bc2a2acf3fa222500895f6b",
        "body": "I like your angle on this. I hadn't thought about it that way!",
        "lang": "en",
        "upvotes": 506,
        "downvotes": 43
    },
    {
        "author": "Amado",
        "postId": "5bc2a2acf3fa222500895f6c",
        "body": "This is going to be really useful for people. Great job!",
        "lang": "en",
        "upvotes": 410,
        "downvotes": 130
    },
    {
        "author": "Alfie",
        "postId": "5bc2a2acf3fa222500895f6c",
        "body": "If I had this guide when I moved here, I would have landed on my feet!",
        "lang": "en",
        "upvotes": 326,
        "downvotes": 33
    },
    {
        "author": "Chad",
        "postId": "5bc2a2acf3fa222500895f6c",
        "body": "I wish I had know this when I moved here!",
        "lang": "en",
        "upvotes": 1037,
        "downvotes": 158
    },
    {
        "author": "Herb",
        "postId": "5bc2a2acf3fa222500895f6c",
        "body": "Nice! Well done!",
        "lang": "en",
        "upvotes": 275,
        "downvotes": 153
    },
    {
        "author": "Orville",
        "postId": "5bc2a2acf3fa222500895f6d",
        "body": "This guide is very well put together.",
        "lang": "en",
        "upvotes": 870,
        "downvotes": 195
    },
    {
        "author": "Anuja",
        "postId": "5bc2a2acf3fa222500895f6d",
        "body": "Nice! Well done!",
        "lang": "en",
        "upvotes": 1160,
        "downvotes": 5
    },
    {
        "author": "Charles",
        "postId": "5bc2a2acf3fa222500895f6d",
        "body": "YES!",
        "lang": "en",
        "upvotes": 602,
        "downvotes": 110
    },
    {
        "author": "Dennis",
        "postId": "5bc2a2acf3fa222500895f6e",
        "body": "I'm sending this to my cousin. He's been asking me about this!",
        "lang": "en",
        "upvotes": 595,
        "downvotes": 186
    }
];
