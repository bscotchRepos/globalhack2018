module.exports = [
    {
        "author": "Ariel",
        "postId": "5bc2a2acf3fa222500895f62",
        "body": "You can go to any bank in your area and talk to a banker there. I'd recommend using Google Maps to find the nearest bank to you. If you see a bank called a \"Community Credit Union\", that means it's a smaller, local bank, while banks that aren't called \"Credit Union\" or similar will probably be branches of larger, national banks. Whichever bank you choose probably doesn't matter that much. You'll need a small amount of money to open an account. Once you have the account, you will be able to order checks directly from the bank, get a credit card, and set up other accounts as well. All you need to do is ask the banker for any of these services, and they will get you set up.",
        "lang": "en",
        "upvotes": 254,
        "downvotes": 2
    },
    {
        "author": "Vanessa",
        "postId": "5bc2a2acf3fa222500895f63",
        "body": "You will probably need a bank account. Employers will usually pay by checks, which you need to bring to a bank to deposit, or they will pay by \"direct deposit,\" which means they will electronically transfer money into your bank account.",
        "lang": "en",
        "upvotes": 950,
        "downvotes": 17
    },
    {
        "author": "Russel",
        "postId": "5bc2a2acf3fa222500895f64",
        "body": "Go to USCIS.gov. That's the web site for United States Citizenship and Immigration Services, and they should have all the info you need.",
        "lang": "en",
        "upvotes": 882,
        "downvotes": 51
    },
    {
        "author": "Tanya",
        "postId": "5bc2a2acf3fa222500895f65",
        "body": "Find a CPA in your area who specializes in personal taxes. CPA stands for Certified Public Accountant. Generally, you will pay a CPA something like a couple hundred dollars to go through your finances and make sure you have reported everything properly.",
        "lang": "en",
        "upvotes": 1001,
        "downvotes": 15
    },
    {
        "author": "Alfie",
        "postId": "5bc2a2acf3fa222500895f65",
        "body": "Taxes are due April 15, although you can file for an extension. Everything is way easier if you get a good accountant, though, so I'd recommend starting there!",
        "lang": "en",
        "upvotes": 241,
        "downvotes": 154
    },
    {
        "author": "Winford",
        "postId": "5bc2a2acf3fa222500895f65",
        "body": "There are lots of different kinds of taxes, but many of them happen automatically. For example, you will pay sales tax on lots of transactions, like when you buy items at stores. That tax happens automatically, and you don't have to do anything about it. The big one is the Federal Income Tax, which you'll want to get a CPA to help you with.",
        "lang": "en",
        "upvotes": 1143,
        "downvotes": 66
    },
    {
        "author": "Suzette",
        "postId": "5bc2a2acf3fa222500895f66",
        "body": "The first place to look is an online site like ziprecruiter.com or indeed.com. Lots of companies post a huge range of jobs there. When you apply, you will need to create a resume and a cover letter. The resume is a document that is a concise detailing of who you are, what your skills are, and your personal history. The cover letter is one-page letter that introduces you to the company and explains why you think they should hire you.",
        "lang": "en",
        "upvotes": 811,
        "downvotes": 96
    },
    {
        "author": "Susan",
        "postId": "5bc2a2acf3fa222500895f66",
        "body": "Make sure your visa status is in order and that you are legally eligible to work in the country. Some employers worry about that, so if you can anticipate that and be very up-front about it right at the start, you'll have an advantage.",
        "lang": "en",
        "upvotes": 757,
        "downvotes": 170
    },
    {
        "author": "Cassandra",
        "postId": "5bc2a2acf3fa222500895f67",
        "body": "Sometimes you will rent an apartment that will have a washer and dryer in the building, or in the apartment complex. In those cases, you'll usually need to bring some quarters to feed into the machine to wash each load. If you don't have access to that, you can bring your clothes to a laundromat, which is a shop that has a whole bunch of washers and dryers init. You'll need to bring quarters there too, probably.",
        "lang": "en",
        "upvotes": 781,
        "downvotes": 146
    },
    {
        "author": "Raul",
        "postId": "5bc2a2acf3fa222500895f67",
        "body": "You're going to need a lot of quarters. Ideally, ask for rolls of quarters when you go to a bank. If you can't make it to a bank, you can sometimes go to the customer service desk at a grocery store, and they'll get you the quarters -- but not always.",
        "lang": "en",
        "upvotes": 542,
        "downvotes": 73
    },
    {
        "author": "Rupert",
        "postId": "5bc2a2acf3fa222500895f68",
        "body": "If you don't have a car, you'll want to use public transportation. The best way to figure that out is to Google the website for the Department of Transportation for that state or city, and they should have a bunch of information about it. Google Maps now also has public transportation options, which can help you figure out which bus stops you need to go to, or which train stations.",
        "lang": "en",
        "upvotes": 806,
        "downvotes": 191
    },
    {
        "author": "Vanessa",
        "postId": "5bc2a2acf3fa222500895f69",
        "body": "Most small towns don't have much for public transportation, so you'll need to either get a car, make friends who have cars, carpool to work, or try to find a place close enough to your work that you can walk or bike.",
        "lang": "en",
        "upvotes": 1043,
        "downvotes": 70
    },
    {
        "author": "Takisha",
        "postId": "5bc2a2acf3fa222500895f6a",
        "body": "I can't believe I didn't know this!",
        "lang": "en",
        "upvotes": 202,
        "downvotes": 154
    },
    {
        "author": "Carmelo",
        "postId": "5bc2a2acf3fa222500895f6a",
        "body": "Nice! Well done!",
        "lang": "en",
        "upvotes": 350,
        "downvotes": 143
    },
    {
        "author": "Alfie",
        "postId": "5bc2a2acf3fa222500895f6a",
        "body": "YES!",
        "lang": "en",
        "upvotes": 376,
        "downvotes": 134
    },
    {
        "author": "Alissa",
        "postId": "5bc2a2acf3fa222500895f6a",
        "body": "I can't believe I didn't know this!",
        "lang": "en",
        "upvotes": 687,
        "downvotes": 139
    },
    {
        "author": "Nina",
        "postId": "5bc2a2acf3fa222500895f6a",
        "body": "This guide is very well put together.",
        "lang": "en",
        "upvotes": 405,
        "downvotes": 154
    },
    {
        "author": "Nicholas",
        "postId": "5bc2a2acf3fa222500895f6b",
        "body": "YES!",
        "lang": "en",
        "upvotes": 876,
        "downvotes": 86
    },
    {
        "author": "Blanch",
        "postId": "5bc2a2acf3fa222500895f6b",
        "body": "This is really well written!",
        "lang": "en",
        "upvotes": 609,
        "downvotes": 25
    },
    {
        "author": "Jere",
        "postId": "5bc2a2acf3fa222500895f6b",
        "body": "I'm glad this information is getting out there.",
        "lang": "en",
        "upvotes": 1187,
        "downvotes": 175
    },
    {
        "author": "Amado",
        "postId": "5bc2a2acf3fa222500895f6b",
        "body": "I like your angle on this. I hadn't thought about it that way!",
        "lang": "en",
        "upvotes": 506,
        "downvotes": 43
    },
    {
        "author": "Amado",
        "postId": "5bc2a2acf3fa222500895f6c",
        "body": "This is going to be really useful for people. Great job!",
        "lang": "en",
        "upvotes": 410,
        "downvotes": 130
    },
    {
        "author": "Alfie",
        "postId": "5bc2a2acf3fa222500895f6c",
        "body": "If I had this guide when I moved here, I would have landed on my feet!",
        "lang": "en",
        "upvotes": 326,
        "downvotes": 33
    },
    {
        "author": "Chad",
        "postId": "5bc2a2acf3fa222500895f6c",
        "body": "I wish I had know this when I moved here!",
        "lang": "en",
        "upvotes": 1037,
        "downvotes": 158
    },
    {
        "author": "Herb",
        "postId": "5bc2a2acf3fa222500895f6c",
        "body": "Nice! Well done!",
        "lang": "en",
        "upvotes": 275,
        "downvotes": 153
    },
    {
        "author": "Orville",
        "postId": "5bc2a2acf3fa222500895f6d",
        "body": "This guide is very well put together.",
        "lang": "en",
        "upvotes": 870,
        "downvotes": 195
    },
    {
        "author": "Anuja",
        "postId": "5bc2a2acf3fa222500895f6d",
        "body": "Nice! Well done!",
        "lang": "en",
        "upvotes": 1160,
        "downvotes": 5
    },
    {
        "author": "Charles",
        "postId": "5bc2a2acf3fa222500895f6d",
        "body": "YES!",
        "lang": "en",
        "upvotes": 602,
        "downvotes": 110
    },
    {
        "author": "Dennis",
        "postId": "5bc2a2acf3fa222500895f6e",
        "body": "I'm sending this to my cousin. He's been asking me about this!",
        "lang": "en",
        "upvotes": 595,
        "downvotes": 186
    },
    {
        "author": "Ariel",
        "postId": "5bc2c6655de3530878e97bde",
        "body": "Вы можете пойти в любой банк в вашем районе и поговорить с банкиром. Я бы рекомендовал использовать Карты Google, чтобы найти ближайший к вам банк. Если вы видите банк под названием «Community Credit Union», это означает, что это меньший местный банк, а банки, которые не называются «Кредитный союз» или подобные, вероятно, будут филиалами более крупных национальных банков. Какой бы банк вы ни выбрали, это не так важно. Для открытия учетной записи вам потребуется небольшая сумма денег. После того, как у вас есть учетная запись, вы сможете заказать чеки непосредственно из банка, получить кредитную карту и настроить другие учетные записи. Все, что вам нужно сделать, это попросить банкира о любой из этих услуг, и они вас устроят.",
        "lang": "ru",
        "upvotes": 254,
        "downvotes": 2
    },
    {
        "author": "Ariel",
        "postId": "5bc2c6655de3530878e97bdf",
        "body": "Puede ir a cualquier banco en su área y hablar con un banquero allí. Recomiendo usar Google Maps para encontrar el banco más cercano a usted. Si ve un banco llamado \"Community Credit Union\", eso significa que es un banco local más pequeño, mientras que los bancos que no se llaman \"Credit Union\" o similares serán probablemente sucursales de bancos nacionales más grandes. El banco que elija probablemente no importa mucho. Necesitará una pequeña cantidad de dinero para abrir una cuenta. Una vez que tenga la cuenta, podrá pedir cheques directamente del banco, obtener una tarjeta de crédito y configurar otras cuentas también. Todo lo que necesita hacer es pedirle al banquero cualquiera de estos servicios, y ellos lo pondrán en marcha.",
        "lang": "es",
        "upvotes": 254,
        "downvotes": 2
    },
    {
        "author": "Ariel",
        "postId": "5bc2c6655de3530878e97be0",
        "body": "Puede ir a cualquier banco en su área y hablar con un banquero allí. Recomiendo usar Google Maps para encontrar el banco más cercano a usted. Si ve un banco llamado \"Community Credit Union\", eso significa que es un banco local más pequeño, mientras que los bancos que no se llaman \"Credit Union\" o similares serán probablemente sucursales de bancos nacionales más grandes. El banco que elija probablemente no importa mucho. Necesitará una pequeña cantidad de dinero para abrir una cuenta. Una vez que tenga la cuenta, podrá pedir cheques directamente del banco, obtener una tarjeta de crédito y configurar otras cuentas también. Todo lo que necesita hacer es pedirle al banquero cualquiera de estos servicios, y ellos lo pondrán en marcha.",
        "lang": "es",
        "upvotes": 254,
        "downvotes": 2
    },
    {
        "author": "Ariel",
        "postId": "5bc2c6655de3530878e97be1",
        "body": "Puede ir a cualquier banco en su área y hablar con un banquero allí. Recomiendo usar Google Maps para encontrar el banco más cercano a usted. Si ve un banco llamado \"Community Credit Union\", eso significa que es un banco local más pequeño, mientras que los bancos que no se llaman \"Credit Union\" o similares serán probablemente sucursales de bancos nacionales más grandes. El banco que elija probablemente no importa mucho. Necesitará una pequeña cantidad de dinero para abrir una cuenta. Una vez que tenga la cuenta, podrá pedir cheques directamente del banco, obtener una tarjeta de crédito y configurar otras cuentas también. Todo lo que necesita hacer es pedirle al banquero cualquiera de estos servicios, y ellos lo pondrán en marcha.",
        "lang": "es",
        "upvotes": 254,
        "downvotes": 2
    },
    {
        "author": "Ariel",
        "postId": "5bc2c6655de3530878e97be2",
        "body": "يمكنك الذهاب إلى أي بنك في منطقتك والتحدث إلى أحد المصرفيين هناك. أنصحك باستخدام خرائط Google للعثور على أقرب بنك إليك. إذا رأيت مصرفًا يسمى \"الاتحاد الائتماني المجتمعي\" ، فهذا يعني أنه بنك محلي أصغر ، في حين أن البنوك التي لا تُسمى \"اتحاد ائتماني\" أو ما شابهها من المحتمل أن تكون فروعًا للبنوك الوطنية الأكبر. أيا كان البنك الذي تختاره ربما لا يهم كثيرا. ستحتاج إلى مبلغ صغير من المال لفتح حساب. بمجرد حصولك على الحساب ، ستتمكن من طلب شيكات مباشرة من البنك ، والحصول على بطاقة ائتمان ، وإعداد حسابات أخرى أيضًا. كل ما عليك القيام به هو أن تطلب من المصرفي أي من هذه الخدمات ، وسوف تحصل على إعداد.",
        "lang": "ar",
        "upvotes": 254,
        "downvotes": 2
    },
    {
        "author": "Ariel",
        "postId": "5bc2c6655de3530878e97be3",
        "body": "يمكنك الذهاب إلى أي بنك في منطقتك والتحدث إلى أحد المصرفيين هناك. أنصحك باستخدام خرائط Google للعثور على أقرب بنك إليك. إذا رأيت مصرفًا يسمى \"الاتحاد الائتماني المجتمعي\" ، فهذا يعني أنه بنك محلي أصغر ، في حين أن البنوك التي لا تُسمى \"اتحاد ائتماني\" أو ما شابهها من المحتمل أن تكون فروعًا للبنوك الوطنية الأكبر. أيا كان البنك الذي تختاره ربما لا يهم كثيرا. ستحتاج إلى مبلغ صغير من المال لفتح حساب. بمجرد حصولك على الحساب ، ستتمكن من طلب شيكات مباشرة من البنك ، والحصول على بطاقة ائتمان ، وإعداد حسابات أخرى أيضًا. كل ما عليك القيام به هو أن تطلب من المصرفي أي من هذه الخدمات ، وسوف تحصل على إعداد.",
        "lang": "ar",
        "upvotes": 254,
        "downvotes": 2
    },
    {
        "author": "Vanessa",
        "postId": "5bc2c6655de3530878e97be4",
        "body": "Вам, вероятно, понадобится банковский счет. Работодатели обычно платят чеками, которые вам необходимо внести в банк для внесения депозита, или они будут платить «прямым депозитом», что означает, что они будут электронным образом переводить деньги на ваш банковский счет.",
        "lang": "ru",
        "upvotes": 950,
        "downvotes": 17
    },
    {
        "author": "Vanessa",
        "postId": "5bc2c6655de3530878e97be5",
        "body": "Probablemente necesitará una cuenta bancaria. Los empleadores generalmente pagarán con cheques, que deberá llevar a un banco para depositar, o pagarán con \"depósito directo\", lo que significa que transferirán dinero electrónicamente a su cuenta bancaria.",
        "lang": "es",
        "upvotes": 950,
        "downvotes": 17
    },
    {
        "author": "Vanessa",
        "postId": "5bc2c6655de3530878e97be6",
        "body": "Probablemente necesitará una cuenta bancaria. Los empleadores generalmente pagarán con cheques, que deberá llevar a un banco para depositar, o pagarán con \"depósito directo\", lo que significa que transferirán dinero electrónicamente a su cuenta bancaria.",
        "lang": "es",
        "upvotes": 950,
        "downvotes": 17
    },
    {
        "author": "Vanessa",
        "postId": "5bc2c6655de3530878e97be7",
        "body": "Probablemente necesitará una cuenta bancaria. Los empleadores generalmente pagarán con cheques, que deberá llevar a un banco para depositar, o pagarán con \"depósito directo\", lo que significa que transferirán dinero electrónicamente a su cuenta bancaria.",
        "lang": "es",
        "upvotes": 950,
        "downvotes": 17
    },
    {
        "author": "Vanessa",
        "postId": "5bc2c6655de3530878e97be8",
        "body": "ربما ستحتاج إلى حساب مصرفي. عادة ما يدفع أصحاب العمل عن طريق الشيكات ، والتي تحتاج إلى إحضارها إلى أحد البنوك لإيداعها ، أو أنهم سيدفعون عن طريق \"الإيداع المباشر\" ، مما يعني أنهم سيقومون بتحويل الأموال إلكترونيًا إلى حسابك البنكي.",
        "lang": "ar",
        "upvotes": 950,
        "downvotes": 17
    },
    {
        "author": "Vanessa",
        "postId": "5bc2c6655de3530878e97be9",
        "body": "ربما ستحتاج إلى حساب مصرفي. عادة ما يدفع أصحاب العمل عن طريق الشيكات ، والتي تحتاج إلى إحضارها إلى أحد البنوك لإيداعها ، أو أنهم سيدفعون عن طريق \"الإيداع المباشر\" ، مما يعني أنهم سيقومون بتحويل الأموال إلكترونيًا إلى حسابك البنكي.",
        "lang": "ar",
        "upvotes": 950,
        "downvotes": 17
    },
    {
        "author": "Russel",
        "postId": "5bc2c6655de3530878e97bea",
        "body": "Перейдите в USCIS.gov. Это веб-сайт для служб гражданства и иммиграции Соединенных Штатов, и они должны иметь всю необходимую информацию.",
        "lang": "ru",
        "upvotes": 882,
        "downvotes": 51
    },
    {
        "author": "Russel",
        "postId": "5bc2c6655de3530878e97beb",
        "body": "Vaya a USCIS.gov. Ese es el sitio web de los Servicios de Ciudadanía e Inmigración de los Estados Unidos, y deberían tener toda la información que necesita.",
        "lang": "es",
        "upvotes": 882,
        "downvotes": 51
    },
    {
        "author": "Russel",
        "postId": "5bc2c6655de3530878e97bec",
        "body": "Vaya a USCIS.gov. Ese es el sitio web de los Servicios de Ciudadanía e Inmigración de los Estados Unidos, y deberían tener toda la información que necesita.",
        "lang": "es",
        "upvotes": 882,
        "downvotes": 51
    },
    {
        "author": "Russel",
        "postId": "5bc2c6655de3530878e97bed",
        "body": "Vaya a USCIS.gov. Ese es el sitio web de los Servicios de Ciudadanía e Inmigración de los Estados Unidos, y deberían tener toda la información que necesita.",
        "lang": "es",
        "upvotes": 882,
        "downvotes": 51
    },
    {
        "author": "Russel",
        "postId": "5bc2c6655de3530878e97bee",
        "body": "اذهب إلى USCIS.gov. هذا هو موقع الويب الخاص بـ United States Citizenship and Immigration Services ، ويجب أن يكون لديهم كل المعلومات التي تحتاجها.",
        "lang": "ar",
        "upvotes": 882,
        "downvotes": 51
    },
    {
        "author": "Russel",
        "postId": "5bc2c6655de3530878e97bef",
        "body": "اذهب إلى USCIS.gov. هذا هو موقع الويب الخاص بـ United States Citizenship and Immigration Services ، ويجب أن يكون لديهم كل المعلومات التي تحتاجها.",
        "lang": "ar",
        "upvotes": 882,
        "downvotes": 51
    },
    {
        "author": "Tanya",
        "postId": "5bc2c6655de3530878e97bf0",
        "body": "Найдите CPA в своей области, которая специализируется на личных налогах. CPA означает Certified Public Accountant. Как правило, вы заплатите CPA что-то вроде пары сотен долларов, чтобы пройти через свои финансы и убедитесь, что вы сообщили все правильно.",
        "lang": "ru",
        "upvotes": 1001,
        "downvotes": 15
    },
    {
        "author": "Alfie",
        "postId": "5bc2c6655de3530878e97bf0",
        "body": "Налоги должны быть 15 апреля, хотя вы можете подать заявку на продление. Все проще, если вы получите хорошего бухгалтера, поэтому я рекомендую начать там!",
        "lang": "ru",
        "upvotes": 241,
        "downvotes": 154
    },
    {
        "author": "Winford",
        "postId": "5bc2c6655de3530878e97bf0",
        "body": "Существует множество различных видов налогов, но многие из них происходят автоматически. Например, вы будете платить налог с продаж на множество транзакций, например, когда вы покупаете товары в магазинах. Этот налог происходит автоматически, и вам не нужно ничего с этим делать. Большим является Федеральный подоходный налог, который вы захотите получить CPA, чтобы помочь вам.",
        "lang": "ru",
        "upvotes": 1143,
        "downvotes": 66
    },
    {
        "author": "Tanya",
        "postId": "5bc2c6655de3530878e97bf1",
        "body": "Encuentre un CPA en su área que se especialice en impuestos personales. CPA significa Contador Público Certificado. En general, pagará un CPA de unos doscientos dólares para revisar sus finanzas y se asegurará de que haya informado todo correctamente.",
        "lang": "es",
        "upvotes": 1001,
        "downvotes": 15
    },
    {
        "author": "Alfie",
        "postId": "5bc2c6655de3530878e97bf1",
        "body": "Los impuestos vencen el 15 de abril, aunque puede solicitar una extensión. Sin embargo, todo es mucho más fácil si obtienes un buen contador, ¡así que recomiendo comenzar allí!",
        "lang": "es",
        "upvotes": 241,
        "downvotes": 154
    },
    {
        "author": "Winford",
        "postId": "5bc2c6655de3530878e97bf1",
        "body": "Hay muchos tipos diferentes de impuestos, pero muchos de ellos suceden automáticamente. Por ejemplo, pagará el impuesto sobre las ventas en muchas transacciones, como cuando compra artículos en tiendas. Ese impuesto pasa automáticamente, y usted no tiene que hacer nada al respecto. El más importante es el Impuesto sobre la renta federal, en el que querrás obtener un CPA para ayudarte.",
        "lang": "es",
        "upvotes": 1143,
        "downvotes": 66
    },
    {
        "author": "Tanya",
        "postId": "5bc2c6655de3530878e97bf2",
        "body": "Encuentre un CPA en su área que se especialice en impuestos personales. CPA significa Contador Público Certificado. En general, pagará un CPA de unos doscientos dólares para revisar sus finanzas y se asegurará de que haya informado todo correctamente.",
        "lang": "es",
        "upvotes": 1001,
        "downvotes": 15
    },
    {
        "author": "Alfie",
        "postId": "5bc2c6655de3530878e97bf2",
        "body": "Los impuestos vencen el 15 de abril, aunque puede solicitar una extensión. Sin embargo, todo es mucho más fácil si obtienes un buen contador, ¡así que recomiendo comenzar allí!",
        "lang": "es",
        "upvotes": 241,
        "downvotes": 154
    },
    {
        "author": "Winford",
        "postId": "5bc2c6655de3530878e97bf2",
        "body": "Hay muchos tipos diferentes de impuestos, pero muchos de ellos suceden automáticamente. Por ejemplo, pagará el impuesto sobre las ventas en muchas transacciones, como cuando compra artículos en tiendas. Ese impuesto pasa automáticamente, y usted no tiene que hacer nada al respecto. El más importante es el Impuesto sobre la renta federal, en el que querrás obtener un CPA para ayudarte.",
        "lang": "es",
        "upvotes": 1143,
        "downvotes": 66
    },
    {
        "author": "Tanya",
        "postId": "5bc2c6655de3530878e97bf3",
        "body": "Encuentre un CPA en su área que se especialice en impuestos personales. CPA significa Contador Público Certificado. En general, pagará un CPA de unos doscientos dólares para revisar sus finanzas y se asegurará de que haya informado todo correctamente.",
        "lang": "es",
        "upvotes": 1001,
        "downvotes": 15
    },
    {
        "author": "Alfie",
        "postId": "5bc2c6655de3530878e97bf3",
        "body": "Los impuestos vencen el 15 de abril, aunque puede solicitar una extensión. Sin embargo, todo es mucho más fácil si obtienes un buen contador, ¡así que recomiendo comenzar allí!",
        "lang": "es",
        "upvotes": 241,
        "downvotes": 154
    },
    {
        "author": "Winford",
        "postId": "5bc2c6655de3530878e97bf3",
        "body": "Hay muchos tipos diferentes de impuestos, pero muchos de ellos suceden automáticamente. Por ejemplo, pagará el impuesto sobre las ventas en muchas transacciones, como cuando compra artículos en tiendas. Ese impuesto pasa automáticamente, y usted no tiene que hacer nada al respecto. El más importante es el Impuesto sobre la renta federal, en el que querrás obtener un CPA para ayudarte.",
        "lang": "es",
        "upvotes": 1143,
        "downvotes": 66
    },
    {
        "author": "Tanya",
        "postId": "5bc2c6655de3530878e97bf4",
        "body": "ابحث عن اتفاقية شراء في منطقتك متخصصة في الضرائب الشخصية. CPA لتقف على المحاسب العام المعتمد. بشكل عام ، ستدفع CPA شيئًا مثل بضع مئات من الدولارات لتتداول في أمورك المالية وتأكد أنك أبلغت عن كل شيء بشكل صحيح.",
        "lang": "ar",
        "upvotes": 1001,
        "downvotes": 15
    },
    {
        "author": "Alfie",
        "postId": "5bc2c6655de3530878e97bf4",
        "body": "الضرائب مستحقة في 15 أبريل ، على الرغم من أنه يمكنك تقديم طلب للحصول على ملحق. كل شيء أسهل إذا كنت تحصل على محاسب جيد ، على الرغم من ذلك ، فإنني أوصي بالبدء هناك!",
        "lang": "ar",
        "upvotes": 241,
        "downvotes": 154
    },
    {
        "author": "Winford",
        "postId": "5bc2c6655de3530878e97bf4",
        "body": "هناك الكثير من أنواع الضرائب المختلفة ، ولكن الكثير منها يحدث تلقائيًا. على سبيل المثال ، سوف تدفع ضريبة المبيعات على الكثير من المعاملات ، مثل عند شراء سلع في المتاجر. هذه الضريبة تحدث تلقائيًا ، وليس عليك فعل أي شيء حيالها. والأهم هو ضريبة الدخل الفيدرالية ، التي سترغب في الحصول على اتفاقية شراء السلام لمساعدتك.",
        "lang": "ar",
        "upvotes": 1143,
        "downvotes": 66
    },
    {
        "author": "Tanya",
        "postId": "5bc2c6655de3530878e97bf5",
        "body": "ابحث عن اتفاقية شراء في منطقتك متخصصة في الضرائب الشخصية. CPA لتقف على المحاسب العام المعتمد. بشكل عام ، ستدفع CPA شيئًا مثل بضع مئات من الدولارات لتتداول في أمورك المالية وتأكد أنك أبلغت عن كل شيء بشكل صحيح.",
        "lang": "ar",
        "upvotes": 1001,
        "downvotes": 15
    },
    {
        "author": "Alfie",
        "postId": "5bc2c6655de3530878e97bf5",
        "body": "الضرائب مستحقة في 15 أبريل ، على الرغم من أنه يمكنك تقديم طلب للحصول على ملحق. كل شيء أسهل إذا كنت تحصل على محاسب جيد ، على الرغم من ذلك ، فإنني أوصي بالبدء هناك!",
        "lang": "ar",
        "upvotes": 241,
        "downvotes": 154
    },
    {
        "author": "Winford",
        "postId": "5bc2c6655de3530878e97bf5",
        "body": "هناك الكثير من أنواع الضرائب المختلفة ، ولكن الكثير منها يحدث تلقائيًا. على سبيل المثال ، سوف تدفع ضريبة المبيعات على الكثير من المعاملات ، مثل عند شراء سلع في المتاجر. هذه الضريبة تحدث تلقائيًا ، وليس عليك فعل أي شيء حيالها. والأهم هو ضريبة الدخل الفيدرالية ، التي سترغب في الحصول على اتفاقية شراء السلام لمساعدتك.",
        "lang": "ar",
        "upvotes": 1143,
        "downvotes": 66
    },
    {
        "author": "Suzette",
        "postId": "5bc2c6655de3530878e97bf6",
        "body": "Первое место - онлайн-сайт, такой как ziprecruiter.com или Indeed.com. Многие компании отправляют туда огромное количество рабочих мест. Когда вы подаете заявку, вам нужно будет создать резюме и сопроводительное письмо. Резюме - это документ, который является кратким описанием того, кто вы, каковы ваши навыки и ваша личная история. В сопроводительном письме содержится одностраничное письмо, в котором вы познакомитесь с компанией и объясните, почему, по вашему мнению, они должны нанять вас.",
        "lang": "ru",
        "upvotes": 811,
        "downvotes": 96
    },
    {
        "author": "Susan",
        "postId": "5bc2c6655de3530878e97bf6",
        "body": "Убедитесь, что ваш статус визы в порядке и что вы на законных основаниях имеете право работать в стране. Некоторые работодатели беспокоятся об этом, поэтому, если вы можете предвидеть это и быть очень внимательным к этому в самом начале, у вас будет преимущество.",
        "lang": "ru",
        "upvotes": 757,
        "downvotes": 170
    },
    {
        "author": "Suzette",
        "postId": "5bc2c6655de3530878e97bf7",
        "body": "El primer lugar para buscar es un sitio en línea como ziprecruiter.com o indeed.com. Muchas empresas publican una gran variedad de trabajos allí. Cuando solicite, deberá crear un currículum y una carta de presentación. El currículum es un documento que es un resumen conciso de quién es usted, cuáles son sus habilidades y su historia personal. La carta de presentación es una carta de una página que le presenta a la empresa y explica por qué cree que deberían contratarle.",
        "lang": "es",
        "upvotes": 811,
        "downvotes": 96
    },
    {
        "author": "Susan",
        "postId": "5bc2c6655de3530878e97bf7",
        "body": "Asegúrese de que su visa esté en orden y de que sea legalmente elegible para trabajar en el país. Algunos empleadores se preocupan por eso, así que si puedes anticiparte y ser muy directo al principio, tendrás una ventaja.",
        "lang": "es",
        "upvotes": 757,
        "downvotes": 170
    },
    {
        "author": "Suzette",
        "postId": "5bc2c6655de3530878e97bf8",
        "body": "El primer lugar para buscar es un sitio en línea como ziprecruiter.com o indeed.com. Muchas empresas publican una gran variedad de trabajos allí. Cuando solicite, deberá crear un currículum y una carta de presentación. El currículum es un documento que es un resumen conciso de quién es usted, cuáles son sus habilidades y su historia personal. La carta de presentación es una carta de una página que le presenta a la empresa y explica por qué cree que deberían contratarle.",
        "lang": "es",
        "upvotes": 811,
        "downvotes": 96
    },
    {
        "author": "Susan",
        "postId": "5bc2c6655de3530878e97bf8",
        "body": "Asegúrese de que su visa esté en orden y de que sea legalmente elegible para trabajar en el país. Algunos empleadores se preocupan por eso, así que si puedes anticiparte y ser muy directo al principio, tendrás una ventaja.",
        "lang": "es",
        "upvotes": 757,
        "downvotes": 170
    },
    {
        "author": "Suzette",
        "postId": "5bc2c6655de3530878e97bf9",
        "body": "El primer lugar para buscar es un sitio en línea como ziprecruiter.com o indeed.com. Muchas empresas publican una gran variedad de trabajos allí. Cuando solicite, deberá crear un currículum y una carta de presentación. El currículum es un documento que es un resumen conciso de quién es usted, cuáles son sus habilidades y su historia personal. La carta de presentación es una carta de una página que le presenta a la empresa y explica por qué cree que deberían contratarle.",
        "lang": "es",
        "upvotes": 811,
        "downvotes": 96
    },
    {
        "author": "Susan",
        "postId": "5bc2c6655de3530878e97bf9",
        "body": "Asegúrese de que su visa esté en orden y de que sea legalmente elegible para trabajar en el país. Algunos empleadores se preocupan por eso, así que si puedes anticiparte y ser muy directo al principio, tendrás una ventaja.",
        "lang": "es",
        "upvotes": 757,
        "downvotes": 170
    },
    {
        "author": "Suzette",
        "postId": "5bc2c6655de3530878e97bfa",
        "body": "أول مكان للبحث هو موقع على الإنترنت مثل ziprecruiter.com أو really.com. الكثير من الشركات تنشر مجموعة كبيرة من الوظائف هناك. عند التقدم بطلب ، ستحتاج إلى إنشاء سيرة ذاتية ورسالة توضيحية. السيرة الذاتية هي وثيقة تفصيلية موجزة عن هويتك ، ومهاراتك ، وتاريخك الشخصي. خطاب المقدمة هو عبارة عن خطاب من صفحة واحدة يقدمك إلى الشركة ويشرح لك السبب في اعتقادك أنه يجب عليك توظيفك.",
        "lang": "ar",
        "upvotes": 811,
        "downvotes": 96
    },
    {
        "author": "Susan",
        "postId": "5bc2c6655de3530878e97bfa",
        "body": "تأكد من أن حالة التأشيرة الخاصة بك صحيحة وأنك مؤهل قانونًا للعمل في الدولة. بعض أرباب العمل يقلقون من ذلك ، لذلك إذا كنت تستطيع أن تتوقع ذلك وأن تتحدث عنه في البداية ، سيكون لديك ميزة.",
        "lang": "ar",
        "upvotes": 757,
        "downvotes": 170
    },
    {
        "author": "Suzette",
        "postId": "5bc2c6655de3530878e97bfb",
        "body": "أول مكان للبحث هو موقع على الإنترنت مثل ziprecruiter.com أو really.com. الكثير من الشركات تنشر مجموعة كبيرة من الوظائف هناك. عند التقدم بطلب ، ستحتاج إلى إنشاء سيرة ذاتية ورسالة توضيحية. السيرة الذاتية هي وثيقة تفصيلية موجزة عن هويتك ، ومهاراتك ، وتاريخك الشخصي. خطاب المقدمة هو عبارة عن خطاب من صفحة واحدة يقدمك إلى الشركة ويشرح لك السبب في اعتقادك أنه يجب عليك توظيفك.",
        "lang": "ar",
        "upvotes": 811,
        "downvotes": 96
    },
    {
        "author": "Susan",
        "postId": "5bc2c6655de3530878e97bfb",
        "body": "تأكد من أن حالة التأشيرة الخاصة بك صحيحة وأنك مؤهل قانونًا للعمل في الدولة. بعض أرباب العمل يقلقون من ذلك ، لذلك إذا كنت تستطيع أن تتوقع ذلك وأن تتحدث عنه في البداية ، سيكون لديك ميزة.",
        "lang": "ar",
        "upvotes": 757,
        "downvotes": 170
    },
    {
        "author": "Cassandra",
        "postId": "5bc2c6655de3530878e97bfc",
        "body": "Иногда вы арендуете квартиру, в которой есть стиральная и сушильная машины в здании или в комплексе апартаментов. В таких случаях вам, как правило, нужно принести некоторые части для подачи в машину, чтобы мыть каждую нагрузку. Если у вас нет доступа к этому, вы можете принести свою одежду в прачечную, которая представляет собой магазин, в котором есть целая куча стиральных машин и сушилок init. Вероятно, вам тоже нужно принести туда.",
        "lang": "ru",
        "upvotes": 781,
        "downvotes": 146
    },
    {
        "author": "Raul",
        "postId": "5bc2c6655de3530878e97bfc",
        "body": "Тебе понадобится много кварталов. В идеале, попросите рулоны кварталов, когда отправляетесь в банк. Если вы не можете сделать это в банке, вы можете иногда зайти в службу поддержки клиентов в продуктовый магазин, и они доставят вам квартиру, но не всегда.",
        "lang": "ru",
        "upvotes": 542,
        "downvotes": 73
    },
    {
        "author": "Cassandra",
        "postId": "5bc2c6655de3530878e97bfd",
        "body": "A veces, alquilará un apartamento que tendrá una lavadora y secadora en el edificio o en el complejo de apartamentos. En esos casos, generalmente necesitará traer algunos cuartos para alimentar la máquina para lavar cada carga. Si no tiene acceso a eso, puede llevar su ropa a una lavandería, que es una tienda que tiene un montón de lavadoras y secadoras init. Necesitarás llevar cuartos allí también, probablemente.",
        "lang": "es",
        "upvotes": 781,
        "downvotes": 146
    },
    {
        "author": "Raul",
        "postId": "5bc2c6655de3530878e97bfd",
        "body": "Vas a necesitar muchos cuartos. Lo ideal es pedir rollos de monedas cuando vayas a un banco. Si no puede acudir a un banco, a veces puede acudir al servicio de atención al cliente de una tienda de comestibles y le darán los cuartos, pero no siempre.",
        "lang": "es",
        "upvotes": 542,
        "downvotes": 73
    },
    {
        "author": "Cassandra",
        "postId": "5bc2c6655de3530878e97bfe",
        "body": "A veces, alquilará un apartamento que tendrá una lavadora y secadora en el edificio o en el complejo de apartamentos. En esos casos, generalmente necesitará traer algunos cuartos para alimentar la máquina para lavar cada carga. Si no tiene acceso a eso, puede llevar su ropa a una lavandería, que es una tienda que tiene un montón de lavadoras y secadoras init. Necesitarás llevar cuartos allí también, probablemente.",
        "lang": "es",
        "upvotes": 781,
        "downvotes": 146
    },
    {
        "author": "Raul",
        "postId": "5bc2c6655de3530878e97bfe",
        "body": "Vas a necesitar muchos cuartos. Lo ideal es pedir rollos de monedas cuando vayas a un banco. Si no puede acudir a un banco, a veces puede acudir al servicio de atención al cliente de una tienda de comestibles y le darán los cuartos, pero no siempre.",
        "lang": "es",
        "upvotes": 542,
        "downvotes": 73
    },
    {
        "author": "Cassandra",
        "postId": "5bc2c6655de3530878e97bff",
        "body": "A veces, alquilará un apartamento que tendrá una lavadora y secadora en el edificio o en el complejo de apartamentos. En esos casos, generalmente necesitará traer algunos cuartos para alimentar la máquina para lavar cada carga. Si no tiene acceso a eso, puede llevar su ropa a una lavandería, que es una tienda que tiene un montón de lavadoras y secadoras init. Necesitarás llevar cuartos allí también, probablemente.",
        "lang": "es",
        "upvotes": 781,
        "downvotes": 146
    },
    {
        "author": "Raul",
        "postId": "5bc2c6655de3530878e97bff",
        "body": "Vas a necesitar muchos cuartos. Lo ideal es pedir rollos de monedas cuando vayas a un banco. Si no puede acudir a un banco, a veces puede acudir al servicio de atención al cliente de una tienda de comestibles y le darán los cuartos, pero no siempre.",
        "lang": "es",
        "upvotes": 542,
        "downvotes": 73
    },
    {
        "author": "Cassandra",
        "postId": "5bc2c6655de3530878e97c00",
        "body": "في بعض الأحيان سوف تستأجر شقة تحتوي على غسالة ومجفف في المبنى ، أو في المجمع السكني. في تلك الحالات ، ستحتاج عادةً إلى إحضار بعض الأرباع لتغذية الماكينة لغسل كل حمولة. إذا لم يكن لديك إمكانية الوصول إلى ذلك ، يمكنك إحضار ملابسك لغسيل الملابس ، وهو متجر يحتوي على مجموعة كاملة من غسالات ومجففات الملابس. ستحتاج إلى إحضار أرباع هناك أيضًا ، على الأرجح.",
        "lang": "ar",
        "upvotes": 781,
        "downvotes": 146
    },
    {
        "author": "Raul",
        "postId": "5bc2c6655de3530878e97c00",
        "body": "ستحتاج إلى الكثير من الفصول. من الناحية المثالية ، اطلب لفائف من أرباع عندما تذهب إلى أحد البنوك. إذا لم تتمكن من الوصول إلى أحد البنوك ، فيمكنك أحيانًا الذهاب إلى مكتب خدمة العملاء في أحد متاجر البقالة ، وسوف تحصل على النقاط - ولكن ليس دائمًا.",
        "lang": "ar",
        "upvotes": 542,
        "downvotes": 73
    },
    {
        "author": "Cassandra",
        "postId": "5bc2c6655de3530878e97c01",
        "body": "في بعض الأحيان سوف تستأجر شقة تحتوي على غسالة ومجفف في المبنى ، أو في المجمع السكني. في تلك الحالات ، ستحتاج عادةً إلى إحضار بعض الأرباع لتغذية الماكينة لغسل كل حمولة. إذا لم يكن لديك إمكانية الوصول إلى ذلك ، يمكنك إحضار ملابسك لغسيل الملابس ، وهو متجر يحتوي على مجموعة كاملة من غسالات ومجففات الملابس. ستحتاج إلى إحضار أرباع هناك أيضًا ، على الأرجح.",
        "lang": "ar",
        "upvotes": 781,
        "downvotes": 146
    },
    {
        "author": "Raul",
        "postId": "5bc2c6655de3530878e97c01",
        "body": "ستحتاج إلى الكثير من الفصول. من الناحية المثالية ، اطلب لفائف من أرباع عندما تذهب إلى أحد البنوك. إذا لم تتمكن من الوصول إلى أحد البنوك ، فيمكنك أحيانًا الذهاب إلى مكتب خدمة العملاء في أحد متاجر البقالة ، وسوف تحصل على النقاط - ولكن ليس دائمًا.",
        "lang": "ar",
        "upvotes": 542,
        "downvotes": 73
    },
    {
        "author": "Rupert",
        "postId": "5bc2c6655de3530878e97c02",
        "body": "Если у вас нет автомобиля, вы захотите воспользоваться общественным транспортом. Лучший способ понять это - это Google для веб-сайта Департамента транспорта для этого штата или города, и они должны иметь кучу информации об этом. В Google Maps теперь также есть варианты общественного транспорта, которые помогут вам выяснить, на каких автобусах вам нужно ехать или на каких вокзалах.",
        "lang": "ru",
        "upvotes": 806,
        "downvotes": 191
    },
    {
        "author": "Rupert",
        "postId": "5bc2c6655de3530878e97c03",
        "body": "Si no tiene un automóvil, querrá usar el transporte público. La mejor manera de averiguarlo es buscar en Google el sitio web del Departamento de Transporte de ese estado o ciudad, y deben tener un montón de información al respecto. Google Maps ahora también tiene opciones de transporte público, que pueden ayudarlo a determinar a qué paradas de autobús necesita ir, o qué estaciones de tren.",
        "lang": "es",
        "upvotes": 806,
        "downvotes": 191
    },
    {
        "author": "Rupert",
        "postId": "5bc2c6655de3530878e97c04",
        "body": "Si no tiene un automóvil, querrá usar el transporte público. La mejor manera de averiguarlo es buscar en Google el sitio web del Departamento de Transporte de ese estado o ciudad, y deben tener un montón de información al respecto. Google Maps ahora también tiene opciones de transporte público, que pueden ayudarlo a determinar a qué paradas de autobús necesita ir, o qué estaciones de tren.",
        "lang": "es",
        "upvotes": 806,
        "downvotes": 191
    },
    {
        "author": "Rupert",
        "postId": "5bc2c6655de3530878e97c05",
        "body": "Si no tiene un automóvil, querrá usar el transporte público. La mejor manera de averiguarlo es buscar en Google el sitio web del Departamento de Transporte de ese estado o ciudad, y deben tener un montón de información al respecto. Google Maps ahora también tiene opciones de transporte público, que pueden ayudarlo a determinar a qué paradas de autobús necesita ir, o qué estaciones de tren.",
        "lang": "es",
        "upvotes": 806,
        "downvotes": 191
    },
    {
        "author": "Rupert",
        "postId": "5bc2c6655de3530878e97c06",
        "body": "إذا لم يكن لديك سيارة ، فستحتاج إلى استخدام وسائل النقل العام. أفضل طريقة لمعرفة ذلك هي Google للموقع الإلكتروني لوزارة النقل لتلك الولاية أو المدينة ، ويجب أن يكون لديهم مجموعة من المعلومات حول هذا الموضوع. تحتوي خرائط Google الآن أيضًا على خيارات وسائل النقل العام ، والتي يمكن أن تساعدك في معرفة محطات الحافلات التي تحتاج إلى الذهاب إليها ، أو محطات القطار.",
        "lang": "ar",
        "upvotes": 806,
        "downvotes": 191
    },
    {
        "author": "Rupert",
        "postId": "5bc2c6655de3530878e97c07",
        "body": "إذا لم يكن لديك سيارة ، فستحتاج إلى استخدام وسائل النقل العام. أفضل طريقة لمعرفة ذلك هي Google للموقع الإلكتروني لوزارة النقل لتلك الولاية أو المدينة ، ويجب أن يكون لديهم مجموعة من المعلومات حول هذا الموضوع. تحتوي خرائط Google الآن أيضًا على خيارات وسائل النقل العام ، والتي يمكن أن تساعدك في معرفة محطات الحافلات التي تحتاج إلى الذهاب إليها ، أو محطات القطار.",
        "lang": "ar",
        "upvotes": 806,
        "downvotes": 191
    },
    {
        "author": "Vanessa",
        "postId": "5bc2c6655de3530878e97c08",
        "body": "Большинство небольших городов не имеют большого количества для общественного транспорта, поэтому вам нужно либо взять автомобиль, завести друзей, у которых есть автомобили, автобаза на работу, или попытаться найти место, достаточно близко к вашей работе, чтобы вы могли ходить или ездить на велосипеде ,",
        "lang": "ru",
        "upvotes": 1043,
        "downvotes": 70
    },
    {
        "author": "Vanessa",
        "postId": "5bc2c6655de3530878e97c09",
        "body": "La mayoría de las ciudades pequeñas no tienen mucho para el transporte público, por lo que deberá conseguir un automóvil, hacer amigos que tengan automóviles, compartir el automóvil para ir al trabajo o intentar encontrar un lugar lo suficientemente cerca de su trabajo para que pueda caminar o andar en bicicleta. .",
        "lang": "es",
        "upvotes": 1043,
        "downvotes": 70
    },
    {
        "author": "Vanessa",
        "postId": "5bc2c6655de3530878e97c0a",
        "body": "La mayoría de las ciudades pequeñas no tienen mucho para el transporte público, por lo que deberá conseguir un automóvil, hacer amigos que tengan automóviles, compartir el automóvil para ir al trabajo o intentar encontrar un lugar lo suficientemente cerca de su trabajo para que pueda caminar o andar en bicicleta. .",
        "lang": "es",
        "upvotes": 1043,
        "downvotes": 70
    },
    {
        "author": "Vanessa",
        "postId": "5bc2c6655de3530878e97c0b",
        "body": "La mayoría de las ciudades pequeñas no tienen mucho para el transporte público, por lo que deberá conseguir un automóvil, hacer amigos que tengan automóviles, compartir el automóvil para ir al trabajo o intentar encontrar un lugar lo suficientemente cerca de su trabajo para que pueda caminar o andar en bicicleta. .",
        "lang": "es",
        "upvotes": 1043,
        "downvotes": 70
    },
    {
        "author": "Vanessa",
        "postId": "5bc2c6655de3530878e97c0c",
        "body": "لا تملك معظم المدن الصغيرة الكثير من أجل وسائل النقل العام ، لذلك ستحتاج إلى الحصول على سيارة أو تكوين صداقات لديها سيارات أو مرافقي للعمل ، أو محاولة العثور على مكان قريب بما فيه الكفاية لعملك الذي يمكنك المشي فيه أو ركوب الدراجة .",
        "lang": "ar",
        "upvotes": 1043,
        "downvotes": 70
    },
    {
        "author": "Vanessa",
        "postId": "5bc2c6655de3530878e97c0d",
        "body": "لا تملك معظم المدن الصغيرة الكثير من أجل وسائل النقل العام ، لذلك ستحتاج إلى الحصول على سيارة أو تكوين صداقات لديها سيارات أو مرافقي للعمل ، أو محاولة العثور على مكان قريب بما فيه الكفاية لعملك الذي يمكنك المشي فيه أو ركوب الدراجة .",
        "lang": "ar",
        "upvotes": 1043,
        "downvotes": 70
    },
    {
        "author": "Takisha",
        "postId": "5bc2c6655de3530878e97c0e",
        "body": "Не могу поверить, что я этого не знал!",
        "lang": "ru",
        "upvotes": 202,
        "downvotes": 154
    },
    {
        "author": "Carmelo",
        "postId": "5bc2c6655de3530878e97c0e",
        "body": "Ницца! Отлично сработано!",
        "lang": "ru",
        "upvotes": 350,
        "downvotes": 143
    },
    {
        "author": "Alfie",
        "postId": "5bc2c6655de3530878e97c0e",
        "body": "ДА!",
        "lang": "ru",
        "upvotes": 376,
        "downvotes": 134
    },
    {
        "author": "Alissa",
        "postId": "5bc2c6655de3530878e97c0e",
        "body": "Не могу поверить, что я этого не знал!",
        "lang": "ru",
        "upvotes": 687,
        "downvotes": 139
    },
    {
        "author": "Nina",
        "postId": "5bc2c6655de3530878e97c0e",
        "body": "Это руководство очень хорошо сочетается.",
        "lang": "ru",
        "upvotes": 405,
        "downvotes": 154
    },
    {
        "author": "Takisha",
        "postId": "5bc2c6655de3530878e97c0f",
        "body": "¡No puedo creer que no supiera esto!",
        "lang": "es",
        "upvotes": 202,
        "downvotes": 154
    },
    {
        "author": "Carmelo",
        "postId": "5bc2c6655de3530878e97c0f",
        "body": "¡Bonito! ¡Bien hecho!",
        "lang": "es",
        "upvotes": 350,
        "downvotes": 143
    },
    {
        "author": "Alfie",
        "postId": "5bc2c6655de3530878e97c0f",
        "body": "¡SÍ!",
        "lang": "es",
        "upvotes": 376,
        "downvotes": 134
    },
    {
        "author": "Alissa",
        "postId": "5bc2c6655de3530878e97c0f",
        "body": "¡No puedo creer que no supiera esto!",
        "lang": "es",
        "upvotes": 687,
        "downvotes": 139
    },
    {
        "author": "Nina",
        "postId": "5bc2c6655de3530878e97c0f",
        "body": "Esta guía está muy bien puesta.",
        "lang": "es",
        "upvotes": 405,
        "downvotes": 154
    },
    {
        "author": "Takisha",
        "postId": "5bc2c6655de3530878e97c10",
        "body": "¡No puedo creer que no supiera esto!",
        "lang": "es",
        "upvotes": 202,
        "downvotes": 154
    },
    {
        "author": "Carmelo",
        "postId": "5bc2c6655de3530878e97c10",
        "body": "¡Bonito! ¡Bien hecho!",
        "lang": "es",
        "upvotes": 350,
        "downvotes": 143
    },
    {
        "author": "Alfie",
        "postId": "5bc2c6655de3530878e97c10",
        "body": "¡SÍ!",
        "lang": "es",
        "upvotes": 376,
        "downvotes": 134
    },
    {
        "author": "Alissa",
        "postId": "5bc2c6655de3530878e97c10",
        "body": "¡No puedo creer que no supiera esto!",
        "lang": "es",
        "upvotes": 687,
        "downvotes": 139
    },
    {
        "author": "Nina",
        "postId": "5bc2c6655de3530878e97c10",
        "body": "Esta guía está muy bien puesta.",
        "lang": "es",
        "upvotes": 405,
        "downvotes": 154
    },
    {
        "author": "Takisha",
        "postId": "5bc2c6655de3530878e97c11",
        "body": "¡No puedo creer que no supiera esto!",
        "lang": "es",
        "upvotes": 202,
        "downvotes": 154
    },
    {
        "author": "Carmelo",
        "postId": "5bc2c6655de3530878e97c11",
        "body": "¡Bonito! ¡Bien hecho!",
        "lang": "es",
        "upvotes": 350,
        "downvotes": 143
    },
    {
        "author": "Alfie",
        "postId": "5bc2c6655de3530878e97c11",
        "body": "¡SÍ!",
        "lang": "es",
        "upvotes": 376,
        "downvotes": 134
    },
    {
        "author": "Alissa",
        "postId": "5bc2c6655de3530878e97c11",
        "body": "¡No puedo creer que no supiera esto!",
        "lang": "es",
        "upvotes": 687,
        "downvotes": 139
    },
    {
        "author": "Nina",
        "postId": "5bc2c6655de3530878e97c11",
        "body": "Esta guía está muy bien puesta.",
        "lang": "es",
        "upvotes": 405,
        "downvotes": 154
    },
    {
        "author": "Takisha",
        "postId": "5bc2c6655de3530878e97c12",
        "body": "لا أستطيع أن أصدق أنني لم أكن أعرف هذا!",
        "lang": "ar",
        "upvotes": 202,
        "downvotes": 154
    },
    {
        "author": "Carmelo",
        "postId": "5bc2c6655de3530878e97c12",
        "body": "لطيف! أحسنت!",
        "lang": "ar",
        "upvotes": 350,
        "downvotes": 143
    },
    {
        "author": "Alfie",
        "postId": "5bc2c6655de3530878e97c12",
        "body": "نعم فعلا!",
        "lang": "ar",
        "upvotes": 376,
        "downvotes": 134
    },
    {
        "author": "Alissa",
        "postId": "5bc2c6655de3530878e97c12",
        "body": "لا أستطيع أن أصدق أنني لم أكن أعرف هذا!",
        "lang": "ar",
        "upvotes": 687,
        "downvotes": 139
    },
    {
        "author": "Nina",
        "postId": "5bc2c6655de3530878e97c12",
        "body": "هذا الدليل جيدًا.",
        "lang": "ar",
        "upvotes": 405,
        "downvotes": 154
    },
    {
        "author": "Takisha",
        "postId": "5bc2c6655de3530878e97c13",
        "body": "لا أستطيع أن أصدق أنني لم أكن أعرف هذا!",
        "lang": "ar",
        "upvotes": 202,
        "downvotes": 154
    },
    {
        "author": "Carmelo",
        "postId": "5bc2c6655de3530878e97c13",
        "body": "لطيف! أحسنت!",
        "lang": "ar",
        "upvotes": 350,
        "downvotes": 143
    },
    {
        "author": "Alfie",
        "postId": "5bc2c6655de3530878e97c13",
        "body": "نعم فعلا!",
        "lang": "ar",
        "upvotes": 376,
        "downvotes": 134
    },
    {
        "author": "Alissa",
        "postId": "5bc2c6655de3530878e97c13",
        "body": "لا أستطيع أن أصدق أنني لم أكن أعرف هذا!",
        "lang": "ar",
        "upvotes": 687,
        "downvotes": 139
    },
    {
        "author": "Nina",
        "postId": "5bc2c6655de3530878e97c13",
        "body": "هذا الدليل جيدًا.",
        "lang": "ar",
        "upvotes": 405,
        "downvotes": 154
    },
    {
        "author": "Nicholas",
        "postId": "5bc2c6655de3530878e97c14",
        "body": "ДА!",
        "lang": "ru",
        "upvotes": 876,
        "downvotes": 86
    },
    {
        "author": "Blanch",
        "postId": "5bc2c6655de3530878e97c14",
        "body": "Это действительно хорошо написано!",
        "lang": "ru",
        "upvotes": 609,
        "downvotes": 25
    },
    {
        "author": "Jere",
        "postId": "5bc2c6655de3530878e97c14",
        "body": "Я рад, что эта информация выходит.",
        "lang": "ru",
        "upvotes": 1187,
        "downvotes": 175
    },
    {
        "author": "Amado",
        "postId": "5bc2c6655de3530878e97c14",
        "body": "Мне нравится ваш взгляд на это. Я так не думал об этом!",
        "lang": "ru",
        "upvotes": 506,
        "downvotes": 43
    },
    {
        "author": "Nicholas",
        "postId": "5bc2c6655de3530878e97c15",
        "body": "¡SÍ!",
        "lang": "es",
        "upvotes": 876,
        "downvotes": 86
    },
    {
        "author": "Blanch",
        "postId": "5bc2c6655de3530878e97c15",
        "body": "¡Esto está muy bien escrito!",
        "lang": "es",
        "upvotes": 609,
        "downvotes": 25
    },
    {
        "author": "Jere",
        "postId": "5bc2c6655de3530878e97c15",
        "body": "Me alegra que esta información se esté difundiendo.",
        "lang": "es",
        "upvotes": 1187,
        "downvotes": 175
    },
    {
        "author": "Amado",
        "postId": "5bc2c6655de3530878e97c15",
        "body": "Me gusta tu ángulo en esto. ¡No lo había pensado de esa manera!",
        "lang": "es",
        "upvotes": 506,
        "downvotes": 43
    },
    {
        "author": "Nicholas",
        "postId": "5bc2c6655de3530878e97c16",
        "body": "¡SÍ!",
        "lang": "es",
        "upvotes": 876,
        "downvotes": 86
    },
    {
        "author": "Blanch",
        "postId": "5bc2c6655de3530878e97c16",
        "body": "¡Esto está muy bien escrito!",
        "lang": "es",
        "upvotes": 609,
        "downvotes": 25
    },
    {
        "author": "Jere",
        "postId": "5bc2c6655de3530878e97c16",
        "body": "Me alegra que esta información se esté difundiendo.",
        "lang": "es",
        "upvotes": 1187,
        "downvotes": 175
    },
    {
        "author": "Amado",
        "postId": "5bc2c6655de3530878e97c16",
        "body": "Me gusta tu ángulo en esto. ¡No lo había pensado de esa manera!",
        "lang": "es",
        "upvotes": 506,
        "downvotes": 43
    },
    {
        "author": "Nicholas",
        "postId": "5bc2c6655de3530878e97c17",
        "body": "¡SÍ!",
        "lang": "es",
        "upvotes": 876,
        "downvotes": 86
    },
    {
        "author": "Blanch",
        "postId": "5bc2c6655de3530878e97c17",
        "body": "¡Esto está muy bien escrito!",
        "lang": "es",
        "upvotes": 609,
        "downvotes": 25
    },
    {
        "author": "Jere",
        "postId": "5bc2c6655de3530878e97c17",
        "body": "Me alegra que esta información se esté difundiendo.",
        "lang": "es",
        "upvotes": 1187,
        "downvotes": 175
    },
    {
        "author": "Amado",
        "postId": "5bc2c6655de3530878e97c17",
        "body": "Me gusta tu ángulo en esto. ¡No lo había pensado de esa manera!",
        "lang": "es",
        "upvotes": 506,
        "downvotes": 43
    },
    {
        "author": "Nicholas",
        "postId": "5bc2c6655de3530878e97c18",
        "body": "نعم فعلا!",
        "lang": "ar",
        "upvotes": 876,
        "downvotes": 86
    },
    {
        "author": "Blanch",
        "postId": "5bc2c6655de3530878e97c18",
        "body": "هذا هو مكتوب بشكل جيد حقا!",
        "lang": "ar",
        "upvotes": 609,
        "downvotes": 25
    },
    {
        "author": "Jere",
        "postId": "5bc2c6655de3530878e97c18",
        "body": "أنا سعيد لأن هذه المعلومات تنتشر هناك.",
        "lang": "ar",
        "upvotes": 1187,
        "downvotes": 175
    },
    {
        "author": "Amado",
        "postId": "5bc2c6655de3530878e97c18",
        "body": "أنا أحب زاويتك في هذا. لم أفكر في الأمر بهذه الطريقة!",
        "lang": "ar",
        "upvotes": 506,
        "downvotes": 43
    },
    {
        "author": "Nicholas",
        "postId": "5bc2c6655de3530878e97c19",
        "body": "نعم فعلا!",
        "lang": "ar",
        "upvotes": 876,
        "downvotes": 86
    },
    {
        "author": "Blanch",
        "postId": "5bc2c6655de3530878e97c19",
        "body": "هذا هو مكتوب بشكل جيد حقا!",
        "lang": "ar",
        "upvotes": 609,
        "downvotes": 25
    },
    {
        "author": "Jere",
        "postId": "5bc2c6655de3530878e97c19",
        "body": "أنا سعيد لأن هذه المعلومات تنتشر هناك.",
        "lang": "ar",
        "upvotes": 1187,
        "downvotes": 175
    },
    {
        "author": "Amado",
        "postId": "5bc2c6655de3530878e97c19",
        "body": "أنا أحب زاويتك في هذا. لم أفكر في الأمر بهذه الطريقة!",
        "lang": "ar",
        "upvotes": 506,
        "downvotes": 43
    },
    {
        "author": "Amado",
        "postId": "5bc2c6655de3530878e97c1a",
        "body": "Это будет действительно полезно для людей. Прекрасная работа!",
        "lang": "ru",
        "upvotes": 410,
        "downvotes": 130
    },
    {
        "author": "Alfie",
        "postId": "5bc2c6655de3530878e97c1a",
        "body": "Если бы у меня был этот гид, когда я переехал сюда, я бы приземлился на ноги!",
        "lang": "ru",
        "upvotes": 326,
        "downvotes": 33
    },
    {
        "author": "Chad",
        "postId": "5bc2c6655de3530878e97c1a",
        "body": "Хотел бы я знать это, когда я переехал сюда!",
        "lang": "ru",
        "upvotes": 1037,
        "downvotes": 158
    },
    {
        "author": "Herb",
        "postId": "5bc2c6655de3530878e97c1a",
        "body": "Ницца! Отлично сработано!",
        "lang": "ru",
        "upvotes": 275,
        "downvotes": 153
    },
    {
        "author": "Amado",
        "postId": "5bc2c6655de3530878e97c1b",
        "body": "Esto va a ser realmente útil para las personas. ¡Gran trabajo!",
        "lang": "es",
        "upvotes": 410,
        "downvotes": 130
    },
    {
        "author": "Alfie",
        "postId": "5bc2c6655de3530878e97c1b",
        "body": "Si tuviera esta guía cuando me mudé aquí, ¡me habría aterrizado de pie!",
        "lang": "es",
        "upvotes": 326,
        "downvotes": 33
    },
    {
        "author": "Chad",
        "postId": "5bc2c6655de3530878e97c1b",
        "body": "¡Ojalá hubiera sabido esto cuando me mudé aquí!",
        "lang": "es",
        "upvotes": 1037,
        "downvotes": 158
    },
    {
        "author": "Herb",
        "postId": "5bc2c6655de3530878e97c1b",
        "body": "¡Bonito! ¡Bien hecho!",
        "lang": "es",
        "upvotes": 275,
        "downvotes": 153
    },
    {
        "author": "Amado",
        "postId": "5bc2c6655de3530878e97c1c",
        "body": "Esto va a ser realmente útil para las personas. ¡Gran trabajo!",
        "lang": "es",
        "upvotes": 410,
        "downvotes": 130
    },
    {
        "author": "Alfie",
        "postId": "5bc2c6655de3530878e97c1c",
        "body": "Si tuviera esta guía cuando me mudé aquí, ¡me habría aterrizado de pie!",
        "lang": "es",
        "upvotes": 326,
        "downvotes": 33
    },
    {
        "author": "Chad",
        "postId": "5bc2c6655de3530878e97c1c",
        "body": "¡Ojalá hubiera sabido esto cuando me mudé aquí!",
        "lang": "es",
        "upvotes": 1037,
        "downvotes": 158
    },
    {
        "author": "Herb",
        "postId": "5bc2c6655de3530878e97c1c",
        "body": "¡Bonito! ¡Bien hecho!",
        "lang": "es",
        "upvotes": 275,
        "downvotes": 153
    },
    {
        "author": "Amado",
        "postId": "5bc2c6655de3530878e97c1d",
        "body": "Esto va a ser realmente útil para las personas. ¡Gran trabajo!",
        "lang": "es",
        "upvotes": 410,
        "downvotes": 130
    },
    {
        "author": "Alfie",
        "postId": "5bc2c6655de3530878e97c1d",
        "body": "Si tuviera esta guía cuando me mudé aquí, ¡me habría aterrizado de pie!",
        "lang": "es",
        "upvotes": 326,
        "downvotes": 33
    },
    {
        "author": "Chad",
        "postId": "5bc2c6655de3530878e97c1d",
        "body": "¡Ojalá hubiera sabido esto cuando me mudé aquí!",
        "lang": "es",
        "upvotes": 1037,
        "downvotes": 158
    },
    {
        "author": "Herb",
        "postId": "5bc2c6655de3530878e97c1d",
        "body": "¡Bonito! ¡Bien hecho!",
        "lang": "es",
        "upvotes": 275,
        "downvotes": 153
    },
    {
        "author": "Amado",
        "postId": "5bc2c6655de3530878e97c1e",
        "body": "هذا سيكون مفيدا حقا للناس. عمل عظيم!",
        "lang": "ar",
        "upvotes": 410,
        "downvotes": 130
    },
    {
        "author": "Alfie",
        "postId": "5bc2c6655de3530878e97c1e",
        "body": "لو كان لدي هذا الدليل عندما انتقلت إلى هنا ، كنت سأقع على قدمي!",
        "lang": "ar",
        "upvotes": 326,
        "downvotes": 33
    },
    {
        "author": "Chad",
        "postId": "5bc2c6655de3530878e97c1e",
        "body": "أتمنى لو كنت أعرف هذا عندما انتقلت هنا!",
        "lang": "ar",
        "upvotes": 1037,
        "downvotes": 158
    },
    {
        "author": "Herb",
        "postId": "5bc2c6655de3530878e97c1e",
        "body": "لطيف! أحسنت!",
        "lang": "ar",
        "upvotes": 275,
        "downvotes": 153
    },
    {
        "author": "Amado",
        "postId": "5bc2c6655de3530878e97c1f",
        "body": "هذا سيكون مفيدا حقا للناس. عمل عظيم!",
        "lang": "ar",
        "upvotes": 410,
        "downvotes": 130
    },
    {
        "author": "Alfie",
        "postId": "5bc2c6655de3530878e97c1f",
        "body": "لو كان لدي هذا الدليل عندما انتقلت إلى هنا ، كنت سأقع على قدمي!",
        "lang": "ar",
        "upvotes": 326,
        "downvotes": 33
    },
    {
        "author": "Chad",
        "postId": "5bc2c6655de3530878e97c1f",
        "body": "أتمنى لو كنت أعرف هذا عندما انتقلت هنا!",
        "lang": "ar",
        "upvotes": 1037,
        "downvotes": 158
    },
    {
        "author": "Herb",
        "postId": "5bc2c6655de3530878e97c1f",
        "body": "لطيف! أحسنت!",
        "lang": "ar",
        "upvotes": 275,
        "downvotes": 153
    },
    {
        "author": "Orville",
        "postId": "5bc2c6655de3530878e97c20",
        "body": "Это руководство очень хорошо сочетается.",
        "lang": "ru",
        "upvotes": 870,
        "downvotes": 195
    },
    {
        "author": "Anuja",
        "postId": "5bc2c6655de3530878e97c20",
        "body": "Ницца! Отлично сработано!",
        "lang": "ru",
        "upvotes": 1160,
        "downvotes": 5
    },
    {
        "author": "Charles",
        "postId": "5bc2c6655de3530878e97c20",
        "body": "ДА!",
        "lang": "ru",
        "upvotes": 602,
        "downvotes": 110
    },
    {
        "author": "Orville",
        "postId": "5bc2c6655de3530878e97c21",
        "body": "Esta guía está muy bien puesta.",
        "lang": "es",
        "upvotes": 870,
        "downvotes": 195
    },
    {
        "author": "Anuja",
        "postId": "5bc2c6655de3530878e97c21",
        "body": "¡Bonito! ¡Bien hecho!",
        "lang": "es",
        "upvotes": 1160,
        "downvotes": 5
    },
    {
        "author": "Charles",
        "postId": "5bc2c6655de3530878e97c21",
        "body": "¡SÍ!",
        "lang": "es",
        "upvotes": 602,
        "downvotes": 110
    },
    {
        "author": "Orville",
        "postId": "5bc2c6655de3530878e97c22",
        "body": "Esta guía está muy bien puesta.",
        "lang": "es",
        "upvotes": 870,
        "downvotes": 195
    },
    {
        "author": "Anuja",
        "postId": "5bc2c6655de3530878e97c22",
        "body": "¡Bonito! ¡Bien hecho!",
        "lang": "es",
        "upvotes": 1160,
        "downvotes": 5
    },
    {
        "author": "Charles",
        "postId": "5bc2c6655de3530878e97c22",
        "body": "¡SÍ!",
        "lang": "es",
        "upvotes": 602,
        "downvotes": 110
    },
    {
        "author": "Orville",
        "postId": "5bc2c6655de3530878e97c23",
        "body": "Esta guía está muy bien puesta.",
        "lang": "es",
        "upvotes": 870,
        "downvotes": 195
    },
    {
        "author": "Anuja",
        "postId": "5bc2c6655de3530878e97c23",
        "body": "¡Bonito! ¡Bien hecho!",
        "lang": "es",
        "upvotes": 1160,
        "downvotes": 5
    },
    {
        "author": "Charles",
        "postId": "5bc2c6655de3530878e97c23",
        "body": "¡SÍ!",
        "lang": "es",
        "upvotes": 602,
        "downvotes": 110
    },
    {
        "author": "Orville",
        "postId": "5bc2c6655de3530878e97c24",
        "body": "هذا الدليل جيدًا.",
        "lang": "ar",
        "upvotes": 870,
        "downvotes": 195
    },
    {
        "author": "Anuja",
        "postId": "5bc2c6655de3530878e97c24",
        "body": "لطيف! أحسنت!",
        "lang": "ar",
        "upvotes": 1160,
        "downvotes": 5
    },
    {
        "author": "Charles",
        "postId": "5bc2c6655de3530878e97c24",
        "body": "نعم فعلا!",
        "lang": "ar",
        "upvotes": 602,
        "downvotes": 110
    },
    {
        "author": "Orville",
        "postId": "5bc2c6655de3530878e97c25",
        "body": "هذا الدليل جيدًا.",
        "lang": "ar",
        "upvotes": 870,
        "downvotes": 195
    },
    {
        "author": "Anuja",
        "postId": "5bc2c6655de3530878e97c25",
        "body": "لطيف! أحسنت!",
        "lang": "ar",
        "upvotes": 1160,
        "downvotes": 5
    },
    {
        "author": "Charles",
        "postId": "5bc2c6655de3530878e97c25",
        "body": "نعم فعلا!",
        "lang": "ar",
        "upvotes": 602,
        "downvotes": 110
    },
    {
        "author": "Dennis",
        "postId": "5bc2c6655de3530878e97c26",
        "body": "Я посылаю это моему кузену. Он спрашивал меня об этом!",
        "lang": "ru",
        "upvotes": 595,
        "downvotes": 186
    },
    {
        "author": "Dennis",
        "postId": "5bc2c6655de3530878e97c27",
        "body": "Se lo envío a mi primo. ¡Me ha estado preguntando sobre esto!",
        "lang": "es",
        "upvotes": 595,
        "downvotes": 186
    },
    {
        "author": "Dennis",
        "postId": "5bc2c6655de3530878e97c28",
        "body": "Se lo envío a mi primo. ¡Me ha estado preguntando sobre esto!",
        "lang": "es",
        "upvotes": 595,
        "downvotes": 186
    },
    {
        "author": "Dennis",
        "postId": "5bc2c6655de3530878e97c29",
        "body": "Se lo envío a mi primo. ¡Me ha estado preguntando sobre esto!",
        "lang": "es",
        "upvotes": 595,
        "downvotes": 186
    },
    {
        "author": "Dennis",
        "postId": "5bc2c6655de3530878e97c2a",
        "body": "أنا أرسل هذا إلى ابن عمي. لقد سألني عن هذا!",
        "lang": "ar",
        "upvotes": 595,
        "downvotes": 186
    },
    {
        "author": "Dennis",
        "postId": "5bc2c6655de3530878e97c2b",
        "body": "أنا أرسل هذا إلى ابن عمي. لقد سألني عن هذا!",
        "lang": "ar",
        "upvotes": 595,
        "downvotes": 186
    }
]