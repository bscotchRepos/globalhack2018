


const {Translate} = require('@google-cloud/translate'); // Imports the Google Cloud client library

const mongoose = require("mongoose");

const compiled_posts = require('./dummy_data/dummy_posts.js');
const compiled_comments = require('./dummy_data/dummy_comments.js');

const fs = require('fs');

// Your Google Cloud Platform project ID
const projectId = 'twoflags-1539484307167';

// Instantiates a client
const translate = new Translate({
  projectId: projectId,
});

var translateSomething = function(text, language) {
    return translate
    .translate(text, language)
    .then(results => {
        const translation = results[0];
        return translation;
    })
    .catch(err => {
        console.error('ERROR:', err);
        return "";
    });
}

// The text to translate
const new_posts = [];
const new_comments = [];

const language_countries = {
    'ru': ['ru'],
    'es': ['es', 'uy', 'co'],
    'ar': ['ae', 'iq']
}

var translations = {};

var translatePosts = async function() {
    for (var post of compiled_posts) {
        var original_body = post["body"];
        var original_title = post["title"];

        translations[original_body] = {}
        translations[original_title] = {}

        for ( var lang in language_countries) {
            translations[original_body][lang] = await translateSomething(original_body, lang);
            if (original_body != original_title) {
                translations[original_title][lang] = await translateSomething(original_title, lang);
            }   
        }
    }

    for (var comment of compiled_comments) {
        var original_body = comment["body"];

        translations[original_body] = {};

        for ( var lang in language_countries) {
            translations[original_body][lang] = await translateSomething(original_body, lang);
        }
    }

    for (var post of compiled_posts) {
        var original_post_id = post["_id"];
        if (post["lang"] == "en") {
            for (var lang in language_countries) {
                these_countries = language_countries[lang];
               

                for (var country of these_countries) {
                    let new_post_id = mongoose.Types.ObjectId();
                    
                    console.log("Created new post ID:" + new_post_id);

                    let new_post = JSON.parse(JSON.stringify(post));
                    new_post["_id"] = new_post_id;
                    new_post["lang"] = lang;
                    new_post["from"] = country;
                    new_post["title"] = translations[new_post["title"]][lang];   ;
                    new_post["body"] = translations[new_post["body"]][lang];

                    new_posts.push(new_post)

                    // MAKE COMMENTS, TOO!
                    for ( var comment of compiled_comments) {                       
                        if (comment["postId"] == original_post_id) {
                            var new_comment = JSON.parse(JSON.stringify(comment));
                            new_comment["body"] = translations[new_comment["body"]][lang];
                            new_comment["lang"] = lang; 
                            new_comment["postId"] = new_post_id;
                            
                            console.log("   Assigned new post id to comment.")

                            new_comments.push(new_comment); 
                            console.log(new_comment)                
                        }
                    }
                }
            }
        }
    }

    for (var post of new_posts) {
        compiled_posts.push(post);
    }
    for ( var comment of new_comments) {
        compiled_comments.push(comment);
    }
      
    fs.writeFileSync('./dummy_data/dummy_posts_translated.js', JSON.stringify(compiled_posts));
    fs.writeFileSync('./dummy_data/dummy_comments_translated.js', JSON.stringify(compiled_comments));
}

translatePosts();

console.log(new_posts);